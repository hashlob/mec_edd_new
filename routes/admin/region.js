var express = require('express');
var router = express.Router();
var Units = require('../../models/admin/unit');
var department = require('../../models/admin/department');
var helper = require('../../helpers/general');
var validation = require('../../helpers/validation');
var User = require('../../models/admin/user');
//multer object creation
var multer = require('multer')
var storage = multer.diskStorage({
    destination: function (req, file, cb) {
        cb(null, 'public/uploads/')
    },
    filename: function (req, file, cb) {
        var extension = file.originalname.split('.');
        cb(null, Date.now() + "." + extension[1])
    }
})

var upload = multer({storage: storage})

/* GET Add. */
router.get('/add/:id', function (req, res, next) {
    try {


        id = req.params.id;
        // Region.find().where({is_deleted: false}).lean().exec((err, doc) => {
        res.render('admin/region/form', {
            layout: 'admin/layouts/default', parent_id: id, users: req.user.role_id,username:req.user.username,
        })
    }catch (e) {
        redirect('/admin/region');
    }// });
});

/* POST Add. */
router.post('/add', function (req, res, next) {
    data = req.body;
    var valid = validation.validate(data, {
        "name": {
            required: true
        },
        "code": {
            required: true
        },
        "address": {
            required: true,
        },
        
    });
    if (Object.keys(valid).length > 0) {
        error_message = "";
        for (var kv in valid) {
            for (var v in valid[kv]) {
                error_message += valid[kv][v] + "<br/>"
            }
        } 
     
        req.flash('error', error_message);
        
        res.redirect('add/'+data.parent_unit);
    } else {
        var unit = new  Units({
            name: data.name,
            code: data.code,
            address: data.address,
            unit_type: data.unit_type,
            parent_unit:helper.decrypt(data.parent_unit),
            // region_id: helper.decrypt(data.region),
           
        });
        unit.save().then((doc) => {
            req.flash('info', "Successfully added.");
            res.redirect('/admin/region/'+data.parent_unit);
        }, (e) => {
            req.flash('error', e);
            res.redirect('region/add');
        });
    }
})

/* GET Index. */
router.get('/:id', function (req, res, next) {
    var perPage = 10;
    var page = req.param('page') || 1;
    id = req.params.id;
    parent_unit = req.params.id;
     where = {
        is_deleted: false,
    }
     id = helper.decrypt(id);
    if (!id) {
        req.flash('error', "Unable to process your request.");
        res.redirect('/admin/unit');
    } else {
        Units.find({unit_type: 1, parent_unit: id}).sort({'date_added': 'desc'})
            .skip((perPage * page) - perPage)
            .limit(perPage)
            .where(where)
            .lean()
            .exec((err, doc) => {
                if (err) {
                    req.flash('error', "Unable to process your request.");
                    res.redirect('/admin/region');
                } else {
                    Units.count().exec(function (err, count) {
                        res.render('admin/region/index', {
                            layout: 'admin/layouts/default', users: req.user.role_id,username:req.user.username,
                            institutes: helper.manipulate(doc,
                                ['_id'],
                                ['is_active', 'is_deleted']
                            ), parent_id: parent_unit,
                            current: page,
                            pages: Math.ceil(count / perPage)
                        });
                    });
                }
            });
    }
});

/* GET Remove. */
router.post('/remove/:id', function (req, res, next) {
    try {
        id = helper.decrypt(req.params.id);
        if (!id || id === null || id.length === undefined) {
            res.send({
                status: false,
                message: "Unable to process your request."
            });
        } else {
            Units.findById(id, function (err, data) {
                department.find({unit_id:id},function (err,dept_Data) {

                    for(dept_index in dept_Data){

                        User.find({department:dept_Data[dept_index]._id},function (err,usr_Data) {
                            for(user_index in usr_Data){
                                usr_Data[user_index].is_deleted = true;
                                usr_Data[user_index].save();
                            }
                        })
                        dept_Data[dept_index].is_deleted = true;
                        dept_Data[dept_index].save();


                    }

                    Units.find({parent_unit:data._id}, function (err, zone_Data ) {

                        for(zone_index in zone_Data)
                        {
                            department.find({unit_id:zone_Data[zone_index]._id},function (err,dept_Data) {

                                for(dept_index in dept_Data){

                                    User.find({department:dept_Data[dept_index]._id},function (err,usr_Data) {
                                        for(user_index in usr_Data){
                                            usr_Data[user_index].is_deleted = true;
                                            usr_Data[user_index].save();
                                        }
                                    })


                                    dept_Data[dept_index].is_deleted = true;
                                    dept_Data[dept_index].save();


                                }

                                Units.find({parent_unit:zone_Data[zone_index]._id}, function (err, branch_Data ) {

                                    for(branch_index in branch_Data)
                                    {
                                        department.find({unit_id:branch_Data[zone_index]._id},function (err,dept_Data) {

                                            for(dept_index in dept_Data){
                                                User.find({department:dept_Data[dept_index]._id},function (err,usr_Data) {
                                                    for(user_index in usr_Data){
                                                        usr_Data[user_index].is_deleted = true;
                                                        usr_Data[user_index].save();
                                                    }
                                                })


                                                dept_Data[dept_index].is_deleted = true;
                                                dept_Data[dept_index].save();
                                            }

                                        });
                                        branch_Data[branch_index].is_deleted = true;

                                        branch_Data[branch_index].save();
                                    }
                                });
                            });
                            zone_Data[zone_index].is_deleted = true;

                            zone_Data[zone_index].save();
                        }

                        if (!data) {
                            res.send({
                                status: false,
                                message: "Unable to process your request."
                            });
                        } else {
                            data.is_deleted = true;

                            data.save(function (err) {
                                if (err) {
                                    res.send({
                                        status: false,
                                        message: "Unable to process your request."
                                    });
                                } else {
                                    res.send({
                                        status: true,
                                        message: "Successfully Approved"
                                    });
                                }
                            })
                        }

                    });
                });
            })
        }
    }catch (e) {
        redirect('/admin/region');
    }
});

/* GET View Details. */
router.get('/view/:id', function (req, res, next) {
    try {


    id = helper.decrypt(req.params.id);
        if (!id || id === null || id.length === undefined) {
            req.flash('error', "Unable to process your request.");
            res.redirect('/admin/unit');
        } else {
            Units.findById(id, function (err, data) {
            if (!data) {
                req.flash('error', "Unable to process your request.");
                res.redirect('/admin/unit');
            } else {
                res.render('admin/region/view', {
                    layout: 'admin/layouts/default',users:req.user.role_id,username:req.user.username,
                    institute: data
                })
            }
        });
    }}catch (e) {
        redirect('/admin/unit');
    }

});

/* GET Edit. */
router.get('/edit/:id', function (req, res, next) {
    try {


   id = helper.decrypt(req.params.id);
        if (!id || id === null || id.length === undefined) {
       req.flash('error', "Unable to process your request.");
       res.redirect('/admin/unit');
   } else {
       Units.findById(id, function (err, data) {

                res.render('admin/region/edit', {
                    layout: 'admin/layouts/default',users:req.user.role_id,username:req.user.username,
                    institute: data,
                    helper: helper,
                    id: helper.encrypt(data._id.toString())
                })
           
        })
   }}catch (e) {
        redirect('/admin/unit');
    }
});

router.post("/edit/:id", function(req, res, next){
    try {


        id = helper.decrypt(req.params.id);
        if (!id || id === null || id.length === undefined) {
            req.flash('error', "Unable to process your request.");
            res.redirect('/admin/unit');
        } else {
        data = req.body;


            Units.findById(id, function (err, d) {
                if (!d) {
                    req.flash('error', "Unable to process your request.");
                    res.redirect('/admin/region');
                } else {
                    d.name = data.name;
                    d.code = data.code;
                    d.address = data.address;
                    var parent_id = helper.encrypt(d.parent_unit);
                    // d.region_id = helper.decrypt(data.region);
                    d.save(function (err) {
                        if (err) {
                            req.flash('error', "Unable to process your request.");
                            res.redirect('/admin/region/' + parent_id);
                        } else {
                            req.flash('info', "Successfully updated.");
                            res.redirect('/admin/region/' + parent_id);
                        }
                    })
                }
            });
        }
    }
    catch (e) {
        redirect('/admin/unit');
    }
})


module.exports = router;