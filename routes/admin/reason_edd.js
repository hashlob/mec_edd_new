var express = require('express');
var router = express.Router();
var reason_edd = require('../../models/admin/reason_edd');
var helper = require('../../helpers/general');
var validation = require('../../helpers/validation');

//multer object creation
var multer = require('multer')
var storage = multer.diskStorage({
    destination: function (req, file, cb) {
        cb(null, 'public/uploads/')
    },
    filename: function (req, file, cb) {
        var extension = file.originalname.split('.');
        cb(null, Date.now() + "." + extension[1])
    }
})

var upload = multer({storage: storage})

/* GET Add. */
router.get('/add', function (req, res, next) {
    // Region.find().where({is_deleted: false}).lean().exec((err, doc) => {
        res.render('admin/reason_edd/form', {
            layout: 'admin/layouts/default',users:req.user.role_id,username:req.user.username,
        })
    // });
});

/* POST Add. */
router.post('/add', function (req, res, next) {
    data = req.body;
    var valid = validation.validate(data, {
        "name": {
            required: true
        },
      
    });
    if (Object.keys(valid).length > 0) {
        error_message = "";
        for (var kv in valid) {
            for (var v in valid[kv]) {
                error_message += valid[kv][v] + "<br/>"
            }
        }
        req.flash('error', error_message);
        res.redirect('add');
    } else {
        var unit = new reason_edd({
            name: data.name,
           
            // region_id: helper.decrypt(data.region),
           
        });
        unit.save().then((doc) => {
            req.flash('info', "Successfully added.");
            res.redirect('/admin/reason_edd');
        }, (e) => {
            req.flash('error', e);
            res.redirect('reason_edd/add');
        });
    }
})

/* GET Index. */
router.get('/:id?', function (req, res, next) {
    var perPage = 10;
    var page = req.param('page') || 1;
    id = req.params.id;
    where = {
        is_deleted: false,
    }
    if (id != undefined) {
        id = helper.decrypt(id);
        where._id = id;
    }
    reason_edd.find().sort({'date_added': 'desc'})
        .skip((perPage * page) - perPage)
        .limit(perPage)
        .where(where)
        .lean()
        .exec((err, doc) => {
            if (err) {
                req.flash('error', "Unable to process your request.");
                res.redirect('/admin/reason_edd');
            } else {
                reason_edd.count().where(where).exec(function (err, count) {
                    res.render('admin/reason_edd/index', {
                        layout: 'admin/layouts/default',users:req.user.role_id,username:req.user.username,
                        institutes: helper.manipulate(doc,
                            ['_id'],
                            ['is_active', 'is_deleted']
                        ),
                        current: page,
                        pages: Math.ceil(count / perPage)
                    });
                });
            }
        });

});

/* GET Remove. */
router.post('/remove/:id', function (req, res, next) {
    try {
    id = helper.decrypt(req.params.id);
    if (id) {
        reason_edd.findById(id, function (err, data) {

                if (!data) {
                res.send({
                    status: false,
                    message: "Unable to process your request."
                })
            } else {
                data.is_deleted = true;
                data.save(function (err) {
                    if (err) {
                        res.send({
                            status: false,
                            message: "Unable to process your request."
                        })
                    } else {
                        res.send({
                            status: true,
                            message: "Successfully Delete"
                        });
                    }
                })
            }
        });
    } }catch (e) {
        redirect('/admin/reason_edd');
    }
});

/* GET View Details. */
router.get('/view/:id', function (req, res, next) {
    try {


    id = helper.decrypt(req.params.id);

        if (!id || id === null || id.length === undefined) {
            req.flash('error', "Unable to process your request.");
            res.redirect('/admin/reason_edd');
        } else {
        reason_edd.findById(id, function (err, data) {
            if (!data) {
                req.flash('error', "Unable to process your request.");
                res.redirect('/admin/reason_edd');
            } else {
                res.render('admin/reason_edd/view', {
                    layout: 'admin/layouts/default',users:req.user.role_id,username:req.user.username,
                    institute: data
                })
            }
        });
    } }
    catch (e) {
        redirect('/admin/reason_edd');
    }

});

/* GET Edit. */
router.get('/edit/:id', function (req, res, next) {
    id = helper.decrypt(req.params.id);


        if (!id || id === null || id.length === undefined) {
            req.flash('error', "Unable to process your request.");
            res.redirect('/admin/reason_edd');
        } else {
       reason_edd.findById(id, function (err, data) {
       
                res.render('admin/reason_edd/edit', {
                    layout: 'admin/layouts/default',users:req.user.role_id,username:req.user.username,
                  
                    institute: data,
                    helper: helper,
                    id: helper.encrypt(data._id.toString())
                })
           
        })
   }
});

router.post("/edit/:id", function(req, res, next){
    try {


        id = helper.decrypt(req.params.id);
        if (!id || id === null || id.length === undefined) {
            req.flash('error', "Unable to process your request.");
            res.redirect('/admin/reason_edd');
        } else {
        data = req.body;


            reason_edd.findById(id, function (err, d) {
                if (!d) {
                    req.flash('error', "Unable to process your request.");
                    res.redirect('/admin/reason_edd');
                } else {
                    d.name = data.name;
                    // d.region_id = helper.decrypt(data.region);
                    d.save(function (err) {
                        if (err) {
                            req.flash('error', "Unable to process your request.");
                            res.redirect('/admin/reason_edd');
                        } else {
                            req.flash('info', "Successfully updated.");
                            res.redirect('/admin/reason_edd');
                        }
                    })
                }
            });
        }
    }catch (e) {
        redirect('/admin/reason_edd');
    }
})


module.exports = router;