var express = require('express');
var router = express.Router();
var User = require('../../models/admin/user');
var department = require('../../models/admin/department');
var unit = require('../../models/admin/unit');
var helper = require('../../helpers/general');
var validation = require('../../helpers/validation');
var md5 = require('md5');
var passport = require('passport');
module.exports = function (passport) {
    /* GET home page. */
    router.get('/login', function(req, res, next) {
        res.render('admin/auth/login', {
            layout: 'admin/layouts/auth'
        })
    });

    /* GET Index. */

    router.get('/add',async function(req, res, next) {
        var dept = await department.find({is_deleted:false});
        res.render('admin/auth/form', {
            layout: 'admin/layouts/default',depts:dept,users:req.user.role_id,username:req.user.username,
        })
    });
    router.post('/add', function (req, res) {
        var body = req.body,
            username = body.username,
            last_name=body.last_name,
            email= body.email,
            password = body.password,
            department =body.department;
            is_manager = body.is_manager,
        role_id = body.role_id;
        User.findOne({
            username: username
        }, function (err, doc) {
            if (err) {
                res.status(500).send('error occured')
            } else {
                if (doc) {
                    res.status(500).send('Firstname already exists')
                } else {
                    var user = new User()
                    user.username = username;
                    user.last_name = last_name;
                    user.email = email;
                    user.password = user.hashPassword(password);
                    user.department = department;
                    user.role_id = role_id;
                    user.is_manager = is_manager
                    user.save(function (err, user) {
                        if (err) {
                            res.status(500).send('db error')
                        } else {
                            res.redirect('/admin/auth')
                        }
                    })
                }
            }
        })
    });
    /* GET Remove. */
    router.post('/remove/:id', function (req, res, next) {
        try{
            id = helper.decrypt(req.params.id);
            if (id) {
                User.findById(id, function (err, data) {
                    if (!data) {
                        res.send({
                            status: false,
                            message: "Unable to process your request."
                        })
                    } else {
                        data.is_deleted = true;
                        data.save(function (err) {
                            if (err) {
                                res.send({
                                    status: false,
                                    message: "Unable to process your request."
                                })
                            } else {
                                res.send({
                                    status: true,
                                    message: "Successfully Delete"
                                });
                            }
                        })
                    }
                });
            }
        }
        catch (e) {
            redirect('/admin/auth');
        }
    });

    /* GET View Details. */
    router.get('/view/:id', function (req, res, next) {

        try{
            id = helper.decrypt(req.params.id);

            if (!id || id === null || id.length === undefined) {
                req.flash('error', "Unable to process your request.");
                res.redirect('/admin/auth');
            } else {
            User.findById(id, function (err, data) {


                department.find({_id:data.department},function (err,dept_data) {
                    unit.find({_id:dept_data[0].unit_id},function (err,unit_data) {

                        res.render('admin/auth/view', {
                            layout: 'admin/layouts/default',users:req.user.role_id,username:req.user.username,
                            institute: data,
                            department_user:dept_data,
                            unit_user:unit_data
                        })

                    });
                })

            })
            }
        }
        catch (e) {
            redirect('/admin/auth');
        }

    });

    /* GET Edit. */
    router.get('/edit/:id',async function (req, res, next) {
        try{
            id = helper.decrypt(req.params.id);
            if (!id || id === null || id.length === undefined) {
                return res.redirect('/admin/auth/view');
            }
            else {
                var dept = await department.find({is_deleted: false});

                User.findById(id, function (err, data) {

                    res.render('admin/auth/edit', {
                        layout: 'admin/layouts/default', depts: dept, users: req.user.role_id,username:req.user.username,
                        institute: data,
                        helper: helper,
                        id: helper.encrypt(data._id.toString()),
                    })

                })
            }
        }
        catch (e) {
            redirect('/admin/auth');
        }
    });

    router.post("/edit/:id", function(req, res, next){
        try {
            id = helper.decrypt(req.params.id);
            if (!id || id === null || id.length === undefined) {
                return res.redirect('/admin/auth/view');
            }
            else {
                data = req.body;


                User.findById(id, function (err, d) {

                    if (!d) {
                        req.flash('error', "Unable to process your request.");
                        res.redirect('/admin/auth');
                    } else {
                        var user = new User()
                        d.username = data.username;
                        d.last_name = data.last_name;
                        d.email = data.email;
                        if (data.password == "") {

                            d.password = data.old_password;
                        }
                        else {
                            d.password = user.hashPassword(data.password);
                        }
                        d.department = data.department;
                        d.role_id = data.role_id;
                        d.is_manager = data.is_manager;


                        // d.region_id = helper.decrypt(data.region);
                        d.save(function (err) {
                            if (err) {
                                req.flash('error', "Unable to process your request.");
                                res.redirect('/admin/auth');
                            } else {
                                req.flash('info', "Successfully updated.");
                                res.redirect('/admin/auth');
                            }
                        })
                    }
                });
            }
        }
        catch (e) {
            redirect('/admin/auth');
        }

    });

    router.get('/:id?', function (req, res, next) {
        var perPage = 10;
        var page = req.param('page') || 1;
        id = req.params.id;
        where = {
            is_deleted: false,
        }
        if (id != undefined) {
            id = helper.decrypt(id);
            where._id = id;
        }
        User.find().sort({'date_added': 'desc'})
            .skip((perPage * page) - perPage)
            .limit(perPage)
            .where(where)
            .lean()
            .exec((err, doc) => {
                if (err) {
                    req.flash('error', "Unable to process your request.");
                    res.redirect('/admin/auth');
                } else {
                    User.count().where(where).exec(function (err, count) {
                        res.render('admin/auth/index', {
                            layout: 'admin/layouts/default',users:req.user.role_id,username:req.user.username,
                            institutes: helper.manipulate(doc,
                                ['_id'],
                                ['is_active', 'is_deleted']
                            ),
                            current: page,
                            pages: Math.ceil(count / perPage)
                        });
                    });
                }
            });

    });
    router.post('/login', passport.authenticate('local', {
        failureRedirect: '/admin/auth/login',
        successRedirect: '/admin/dashboard',
    }))
    return router;

};
