var express = require('express');
var router = express.Router();
var branch = require('../../models/admin/unit');

var Units = require('../../models/admin/unit');
var department = require('../../models/admin/department');
var helper = require('../../helpers/general');
var validation = require('../../helpers/validation');
var User = require('../../models/admin/user');
//multer object creation
var multer = require('multer')
var storage = multer.diskStorage({
    destination: function (req, file, cb) {
        cb(null, 'public/uploads/')
    },
    filename: function (req, file, cb) {
        var extension = file.originalname.split('.');
        cb(null, Date.now() + "." + extension[1])
    }
})

var upload = multer({storage: storage})

/* GET Add. */
router.get('/add/:id', function (req, res, next) {
    try {
        id = req.params.id;
        // Region.find().where({is_deleted: false}).lean().exec((err, doc) => {
        res.render('admin/branch/form', {
            layout: 'admin/layouts/default', parent_id: id, users: req.user.role_id,region_id:req.query.region_id,zone_id:req.query.zone_id,username:req.user.username,
        })
    }catch (e) {
        redirect('/admin/branch');
    }
    // });
});

/* POST Add. */
router.post('/add', function (req, res, next) {
    data = req.body;
    var valid = validation.validate(data, {
        "name": {
            required: true
        },
        "code": {
            required: true
        },
        "address": {
            required: true,
        },
        
    });
    if (Object.keys(valid).length > 0) {
        error_message = "";
        for (var kv in valid) {
            for (var v in valid[kv]) {
                error_message += valid[kv][v] + "<br/>"
            }
        } 
     
        req.flash('error', error_message);
        
        res.redirect('add/'+data.parent_unit);
    } else {
        var unit = new Units({
            name: data.name,
            code: data.code,
            address: data.address,
            unit_type: data.unit_type,
            parent_unit:helper.decrypt(data.parent_unit),
            // region_id: helper.decrypt(data.region),
           
        });
        unit.save().then((doc) => {
            req.flash('info', "Successfully added.");
            res.redirect('/admin/branch/'+data.parent_unit+"?region_id="+data.region_id+"&zone_id="+data.zone_id);
        }, (e) => {
            req.flash('error', e);
            res.redirect('branch/add');
        });
    }
})

/* GET Index. */
router.get('/:id', function (req, res, next) {
    var perPage = 10;
    var page = req.param('page') || 1;
    id = req.params.id;

    parent_unit = req.params.id;
     where = {
        is_deleted: false,
    }
     id = helper.decrypt(id);
    if (!id) {
        req.flash('error', "Unable to process your request.");
        res.redirect('/admin/unit');
    } else {
        Units.find({unit_type: 3, parent_unit: id}).sort({'date_added': 'desc'})
            .skip((perPage * page) - perPage)
            .limit(perPage)
            .where(where)
            .lean()
            .exec((err, doc) => {
                if (err) {
                    req.flash('error', "Unable to process your request.");
                    res.redirect('/admin/branch');
                } else {
                    Units.count().exec(function (err, count) {
                        res.render('admin/branch/index', {
                            layout: 'admin/layouts/default', users: req.user.role_id,username:req.user.username,
                            institutes: helper.manipulate(doc,
                                ['_id'],
                                ['is_active', 'is_deleted']
                            ), parent_id: parent_unit,region_id: req.query.region_id,zone_id: req.query.zone_id,
                            current: page,
                            pages: Math.ceil(count / perPage)
                        });
                    });
                }
            });
    }
});

/* GET Remove. */
router.post('/remove/:id', function (req, res, next) {
    try {
        id = helper.decrypt(req.params.id);
        if (!id || id === null || id.length === undefined) {
            req.flash('error', "Unable to process your request.");
            res.redirect('/admin/branch');
        } else {
            Units.findById(id, function (err, data) {
                department.find({unit_id:id},function (err,dept_Data) {

                    for(dept_index in dept_Data){

                        User.find({department:dept_Data[dept_index]._id},function (err,usr_Data) {
                            for(user_index in usr_Data){
                                usr_Data[user_index].is_deleted = true;
                                usr_Data[user_index].save();
                            }
                        })
                        dept_Data[dept_index].is_deleted = true;
                        dept_Data[dept_index].save();


                    }
                        if (!data) {
                            req.flash('error', "Unable to process your request.");
                            res.redirect('/admin/branch');
                        } else {
                            data.is_deleted = true;

                            data.save(function (err) {
                                if (err) {
                                    req.flash('error', "Unable to process your request.");
                                    res.redirect('/admin/branch');
                                } else {
                                    req.flash('info', "Successfully deleted.");
                                    res.redirect('/admin/branch');
                                }
                            })
                        }

                    });
                });

        }
    }catch (e) {
        redirect('/admin/zone');
    }
});
/* GET View Details. */
router.get('/view/:id', function (req, res, next) {
    try{
    id = helper.decrypt(req.params.id);
        if (!id || id === null || id.length === undefined) {
            req.flash('error', "Unable to process your request.");
            res.redirect('/admin/unit');
        } else {
            Units.findById(id, function (err, data) {
            if (!data) {
                req.flash('error', "Unable to process your request.");
                res.redirect('/admin/unit');
            } else {
                res.render('admin/branch/view', {
                    layout: 'admin/layouts/default',users:req.user.role_id,username:req.user.username,
                    institute: data
                })
            }
        });
    } }
    catch (e) {
        redirect('/admin/unit');
    }

});

/* GET Edit. */
router.get('/edit/:id', function (req, res, next) {
    try {
        id = helper.decrypt(req.params.id);
        if (!id || id === null || id.length === undefined) {
            req.flash('error', "Unable to process your request.");
            res.redirect('/admin/unit');
        } else {
            Units.findById(id, function (err, data) {
                console.log(data);
                res.render('admin/branch/edit', {
                    layout: 'admin/layouts/default', users: req.user.role_id,username:req.user.username,

                    institute: data,

                    helper: helper,
                    id: helper.encrypt(data._id.toString())
                })

            })
        }
    }
    catch (e) {
        redirect('/admin/unit');
    }
});

router.post("/edit/:id", function(req, res, next){
    try {


        id = helper.decrypt(req.params.id);
        if (!id || id === null || id.length === undefined) {
            req.flash('error', "Unable to process your request.");
            res.redirect('/admin/unit');
        } else {
        data = req.body;


            Units.findById(id, function (err, d) {
                if (!d) {
                    req.flash('error', "Unable to process your request.");
                    res.redirect('/admin/unit');
                } else {
                    d.name = data.name;
                    d.code = data.code;
                    d.address = data.address;
                    var parent_id = helper.encrypt(d.parent_unit);
                    // d.region_id = helper.decrypt(data.region);
                    d.save(function (err) {
                        if (err) {
                            req.flash('error', "Unable to process your request.");
                            res.redirect('/admin/branch/' + parent_id);
                        } else {
                            req.flash('info', "Successfully updated.");
                            res.redirect('/admin/branch/' + parent_id);
                        }
                    })
                }
            });
        }
    }catch (e) {
        redirect('/admin/unit');
    }
})


module.exports = router;