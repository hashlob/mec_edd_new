var express = require('express');
var router = express.Router();
var reporting_line = require('../../models/admin/reporting_line');
var helper = require('../../helpers/general');
var validation = require('../../helpers/validation');
var department = require('../../models/admin/department');
var Unit = require('../../models/admin/unit');
//multer object creation
var multer = require('multer')
var storage = multer.diskStorage({
    destination: function (req, file, cb) {
        cb(null, 'public/uploads/')
    },
    filename: function (req, file, cb) {
        var extension = file.originalname.split('.');
        cb(null, Date.now() + "." + extension[1])
    }
})

var upload = multer({storage: storage})

/* GET Add. */
router.get('/add', function (req, res, next) {
    // Region.find().where({is_deleted: false}).lean().exec((err, doc) => {
    department.find({is_deleted:false},function (err,data) {


        res.render('admin/reporting_line/form', {
            layout: 'admin/layouts/default',users:req.user.role_id,dept_id:data,username:req.user.username,
        })
        // });
    });
});

/* POST Add. */
router.post('/add', function (req, res, next) {
    data = req.body;

    var valid = validation.validate(data, {
        "department_id": {
            required: true
        },
        "unit_id": {
            required: true
        },
        "sort": {
            required: true
        },
    });
    if (Object.keys(valid).length > 0) {
        error_message = "";
        for (var kv in valid) {
            for (var v in valid[kv]) {
                error_message += valid[kv][v] + "<br/>"
            }
        }
        req.flash('error', error_message);
        res.redirect('add');
    } else {

        var qt = [];
        for(index in data.question)
     {

        qt.push({
             question:data.question[index],
             options:data.option,
         }) ;

    }


        var unit = new reporting_line({

            department_id: data.department_id,
            unit_id: data.unit_id,
            sort: data.sort,
            is_attachment:data.is_attachment,
            questions:qt,

            // region_id: helper.decrypt(data.region),

        });
        unit.save().then((doc) => {
            req.flash('info', "Successfully added.");
            res.redirect('/admin/reporting_line');
        }, (e) => {
            req.flash('error', e);
            res.redirect('reporting_line/add');
        });
    }
})

router.get('/department',function (req,res) {
    var unit_id = req.query.department_id;

    Unit.find({_id:unit_id},function (req,data) {
        res.send(data);

    });

})
/* GET Index. */
router.get('/:id?', function (req, res, next) {
    var perPage = 10;
    var page = req.param('page') || 1;
    id = req.params.id;
    where = {
        is_deleted: false,
    }
    if (id != undefined) {
        id = helper.decrypt(id);
        where._id = id;
    }


    reporting_line.find().sort({'date_added': 'desc'})
        .skip((perPage * page) - perPage)
        .limit(perPage)
        .where(where)
        .lean()
        .exec((err, doc) => {
            if (err) {
                req.flash('error', "Unable to process your request.");
                res.redirect('/admin/reporting_line');
            } else {
                reporting_line.count().where(where).exec(function (err, count) {

                    department.find({},function (err,dept_data) {
                        Unit.find({},function (err,unit_data) {


                            res.render('admin/reporting_line/index', {
                                layout: 'admin/layouts/default',users:req.user.role_id,department_data:dept_data,unit__ids:unit_data,username:req.user.username,
                                institutes: helper.manipulate(doc,
                                    ['_id'],
                                    ['is_active', 'is_deleted']
                                ),
                                current: page,
                                pages: Math.ceil(count / perPage)
                            });
                        });
                    })
                })
            }

        });

});

/* GET Remove. */
router.post('/remove/:id', function (req, res, next) {

    try {
        id = helper.decrypt(req.params.id);
        console.log(id);
        if (id) {
            reporting_line.findById(id, function (err, data) {

                if (!data ) {

                    res.send({
                        status: false,
                        message: "Unable to process your request."
                    })
                } else {

                    data.remove(function (err) {
                        if (err) {
                            res.send({
                                status: false,
                                message: "Unable to process your request."
                            })
                        } else {
                            res.send({
                                status: true,
                                message: "Successfully Approved"
                            });
                        }
                    })
                }
            });
        } }
    catch (e) {
        redirect('/admin/reporting_line');
    }
});

/* GET View Details. */
router.get('/view/:id', function (req, res, next) {
    try {


        id = helper.decrypt(req.params.id);
        if (!id || id === null || id.length === undefined) {
            req.flash('error', "Unable to process your request.");
            res.redirect('/admin/reporting_line');
        } else {
            reporting_line.findById(id, function (err, data) {
                if (!data) {
                    req.flash('error', "Unable to process your request.");
                    res.redirect('/admin/reporting_line');
                } else {
                    res.render('admin/reporting_line/view', {
                        layout: 'admin/layouts/default',users:req.user.role_id,username:req.user.username,
                        institute: data
                    })
                }
            });
        } }catch (e) {
        redirect('/admin/reporting_line');
    }
});

/* GET Edit. */
router.get('/edit/:id', function (req, res, next) {
    try {


        id = helper.decrypt(req.params.id);
        if (!id || id === null || id.length === undefined) {
            req.flash('error', "Unable to process your request.");
            res.redirect('/admin/reporting_line');
        } else {
            reporting_line.findById(id, function (err, data) {
                department.find({}, function (err, dept_data) {
                    res.render('admin/reporting_line/edit', {
                        layout: 'admin/layouts/default', users: req.user.role_id, dept_id: dept_data,username:req.user.username,
                        institute: data,
                        helper: helper,
                        id: helper.encrypt(data._id.toString())
                    })

                })
            })
        }
    }
    catch (e) {
        redirect('/admin/reporting_line');
    }
});

router.post("/edit/:id", function(req, res, next){
    try {


        id = helper.decrypt(req.params.id);
        data = req.body;

        if (!id || id === null || id.length === undefined) {
            req.flash('error', "Unable to process your request.");
            res.redirect('/admin/reporting_line');
        } else {
            reporting_line.findById(id, function (err, d) {
                if (!d) {
                    req.flash('error', "Unable to process your request.");
                    res.redirect('/admin/reporting_line');
                } else {
                    if (data.department_id == " ") {
                        d.department_id = data.old_department_id;
                    }
                    else {
                        d.department_id = data.department_id;
                    }
                    if (data.unit_id == " ") {
                        d.unit_id = data.old_unit_id;
                    }
                    else {
                        d.unit_id = data.unit_id;
                    }


                    d.sort = data.sort;
                    if (data.is_attachment == undefined) {
                        d.is_attachment = false;
                    }
                    else {
                        d.is_attachment = data.is_attachment;

                    }// d.region_id = helper.decrypt(data.region);
                    d.save(function (err) {
                        if (err) {
                            req.flash('error', "Unable to process your request.");
                            res.redirect('/admin/reporting_line');
                        } else {
                            req.flash('info', "Successfully updated.");
                            res.redirect('/admin/reporting_line');
                        }
                    })
                }
            });
        }
    }
    catch (e) {
        redirect('/admin/reporting_line');
    }
})


module.exports = router;