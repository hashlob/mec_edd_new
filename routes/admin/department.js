var express = require('express');
var router = express.Router();
var department = require('../../models/admin/department');

var helper = require('../../helpers/general');
var validation = require('../../helpers/validation');
var User = require('../../models/admin/user');
//multer object creation
var multer = require('multer')
var storage = multer.diskStorage({
    destination: function (req, file, cb) {
        cb(null, 'public/uploads/')
    },
    filename: function (req, file, cb) {
        var extension = file.originalname.split('.');
        cb(null, Date.now() + "." + extension[1])
    }
})

var upload = multer({storage: storage})

/* GET Add. */
router.get('/add/:id', function (req, res, next) {
    try {


        id = req.params.id;
        // Region.find().where({is_deleted: false}).lean().exec((err, doc) => {
        res.render('admin/department/form', {
            layout: 'admin/layouts/default', users: req.user.role_id,username:req.user.username, parent_id: id
        })
    }catch (e) {
        redirect('/admin/department');
    }
    // });
});

/* POST Add. */
router.post('/add', function (req, res, next) {
    data = req.body;
    var valid = validation.validate(data, {
        "name": {
            required: true
        },


    });
    if (Object.keys(valid).length > 0) {
        error_message = "";
        for (var kv in valid) {
            for (var v in valid[kv]) {
                error_message += valid[kv][v] + "<br/>"
            }
        }

        req.flash('error', error_message);

        res.redirect('add/'+data.unit_id);
    } else {
        var unit = new department({
            name: data.name,
            unit_id:helper.decrypt(data.unit_id),
            // region_id: helper.decrypt(data.region),

        });
        unit.save().then((doc) => {
            req.flash('info', "Successfully added.");
            res.redirect('/admin/department/'+data.unit_id);
        }, (e) => {
            req.flash('error', e);
            res.redirect('department/add');
        });
    }
})

/* GET Index. */
router.get('/:id', function (req, res, next) {
    var perPage = 10;
    var page = req.param('page') || 1;
    id = req.params.id;
    unit_id = req.params.id;
    where = {
        is_deleted: false,
    }
    id = helper.decrypt(id);
    if (!id) {
        req.flash('error', "Unable to process your request.");
        res.redirect('/admin/unit');
    } else {
        department.find({unit_id: id}).sort({'date_added': 'desc'})
            .skip((perPage * page) - perPage)
            .limit(perPage)
            .where(where)
            .lean()
            .exec((err, doc) => {
                if (err) {
                    req.flash('error', "Unable to process your request.");
                    res.redirect('/admin/department');
                } else {
                    department.count().exec(function (err, count) {
                        res.render('admin/department/index', {
                            layout: 'admin/layouts/default', users: req.user.role_id,username:req.user.username,
                            institutes: helper.manipulate(doc,
                                ['_id'],
                                ['is_active', 'is_deleted']
                            ), parent_id: unit_id,region_id: req.query.region_id,zone_id: req.query.zone_id,branch_id: req.query.branch_id,
                            current: page,
                            pages: Math.ceil(count / perPage)
                        });
                    });
                }
            });
    }
});

/* GET Remove. */
router.post('/remove/:id', function (req, res, next) {
    try {

        id = helper.decrypt(req.params.id);

        if (!id || id === null || id.length === undefined) {
            res.send({
                status: false,
                message: "Unable to process your request."
            });
        } else {

            department.findById(id,function (err,data) {


                    User.find({department:data._id},function (err,usr_Data) {

                        for(user_index in usr_Data){

                        console.log(usr_Data[user_index]);
                            usr_Data[user_index].is_deleted = true;
                            usr_Data[user_index].save();
                        }
                    })


                if (!data) {
                    res.send({
                        status: false,
                        message: "Unable to process your request."
                    });
                } else {
                    data.is_deleted = true;

                    data.save(function (err) {
                        if (err) {
                            res.send({
                                status: false,
                                message: "Unable to process your request."
                            });
                        } else {
                            res.send({
                                status: true,
                                message: "Successfully Approved"
                            });
                        }
                    })
                }
            });

        }
    }
    catch (e) {
        redirect('/admin/department');
    }
});

/* GET View Details. */
router.get('/view/:id', function (req, res, next) {
    try {


        id = helper.decrypt(req.params.id);
        if (!id || id === null || id.length === undefined) {
            req.flash('error', "Unable to process your request.");
            res.redirect('/admin/unit');
        } else {
            department.findById(id, function (err, data) {
                if (!data) {
                    req.flash('error', "Unable to process your request.");
                    res.redirect('/admin/unit');
                } else {
                    res.render('admin/department/view', {
                        layout: 'admin/layouts/default', users: req.user.role_id,username:req.user.username,
                        institute: data
                    })
                }
            });
        }
    }
    catch (e) {
        redirect('/admin/unit');
    }

});

/* GET Edit. */
router.get('/edit/:id', function (req, res, next) {
    try {


        id = helper.decrypt(req.params.id);
        if (!id || id === null || id.length === undefined) {
            req.flash('error', "Unable to process your request.");
            res.redirect('/admin/unit');
        } else {
            department.findById(id, function (err, data) {
                console.log(data);
                res.render('admin/department/edit', {
                    layout: 'admin/layouts/default', users: req.user.role_id,username:req.user.username,

                    institute: data,

                    helper: helper,
                    id: helper.encrypt(data._id.toString())
                })

            })
        }
    }
    catch (e) {
        redirect('/admin/unit');
    }
});

router.post("/edit/:id", function(req, res, next){
    try {
        id = helper.decrypt(req.params.id);
        if (!id || id === null || id.length === undefined) {
            req.flash('error', "Unable to process your request.");
            res.redirect('/admin/unit');
        } else {
            data = req.body;


            department.findById(id, function (err, d) {
                if (!d) {
                    req.flash('error', "Unable to process your request.");
                    res.redirect('/admin/unit');
                } else {
                    d.name = data.name;
                    var parent_id = helper.encrypt(d.unit_id);
                    // d.region_id = helper.decrypt(data.region);
                    d.save(function (err) {
                        if (err) {
                            req.flash('error', "Unable to process your request.");
                            res.redirect('/admin/department/' + parent_id);
                        } else {
                            req.flash('info', "Successfully updated.");
                            res.redirect('/admin/department/' + parent_id);
                        }
                    })
                }
            });
        }
    }
    catch (e) {
        redirect('/admin/unit');
    }
})


module.exports = router;