var express = require('express');
const mongoose = require('mongoose');
var router = express.Router();
var Edd = require('../../models/admin/edd');
var customers = require('../../models/admin/customer');
var customer_type = require('../../models/admin/customer_type');
var reason_edd = require('../../models/admin/reason_edd');
var service_type = require('../../models/admin/service_type');
var pep_type = require('../../models/admin/pep_type');
var fund_source = require('../../models/admin/fund_source');
var User = require('../../models/admin/user');
var reporting_line = require('../../models/admin/reporting_line');
var helper = require('../../helpers/general');
var validation = require('../../helpers/validation');
var _ = require('lodash');
var moment = require('moment');
//multer object creation
var fs = require('fs');
var multer = require('multer');
var crypto = require('crypto');
var mime = require('mime');
var pdfMake = require('pdfmake/build/pdfmake.js');
var pdfFonts = require('pdfmake/build/vfs_fonts.js');
pdfMake.vfs = pdfFonts.pdfMake.vfs;


var storage = multer.diskStorage({
    destination: function (req, file, cb) {
        cb(null, './additional_document/')
    },
    filename: function (req, file, cb) {
        crypto.pseudoRandomBytes(16, function (err, raw) {
            cb(null, raw.toString('hex') + Date.now() + '.' + mime.getExtension(file.mimetype));
        });
    }
});

var uploads = multer({storage: storage});



/* PDF. */
router.get('/pdf/:id',function (req,res) {
    try {


        id = helper.decrypt(req.params.id);

        Edd.find({_id:id},function (err,edd_Data) {
            customers.find({_id:edd_Data[0].customer_id},function (err,cust_Data) {
                customer_type.find({_id:cust_Data[0].customer_log[0].customer_type},function(err,customer_type_data){
                    reason_edd.find({_id:{$in:edd_Data[0].reason_of_edd}},function (err,reason_Data) {
                        service_type.find({_id:cust_Data[0].customer_log[0].ocupation.type_of_service},function (err,service_data) {
                            pep_type.find({_id:cust_Data[0].customer_log[0].ocupation.pep_type},function (err,pep_data) {
                                fund_source.find({_id:cust_Data[0].customer_log[0].source_of_fund.source},function (err,fund_source_data) {
                                    var reasondata = [];
                                    for(reason_index in reason_Data)
                                    {
                                        reasondata.push(reason_Data[reason_index].name);
                                    }



                                    var documentDefinition = {
                                        content: [
                                            {
                                                columns: [
                                                    {text: 'Branch Name:', style: 'heading'} ,
                                                    [
                                                        { text:`${edd_Data[0].branch_name}` , margin: [ 0, 0, 0,5 ] },

                                                    ]
                                                ]
                                            },
                                            {
                                                columns: [
                                                    {text: 'EDD Date:', style: 'heading'} ,
                                                    [
                                                        { text:`${edd_Data[0].form_date}` , margin: [ 0, 0, 0,5 ] },

                                                    ]
                                                ]
                                            },

                                            {
                                                columns: [
                                                    {text: 'Voucher No:', style: 'heading'} ,
                                                    [
                                                        { text:`${edd_Data[0].voucher_no}` , margin: [ 0, 0, 0,5 ] },

                                                    ]
                                                ]
                                            },
                                            {
                                                columns: [
                                                    {text: 'Customer Name:', style: 'heading'} ,
                                                    [
                                                        { text:`${cust_Data[0].customer_log[0].customer_name}` , margin: [ 0, 0, 0,5 ] },

                                                    ],
                                                    {text: 'Father\'s/Husband Name:', style: 'heading'} ,
                                                    [
                                                        { text:`${cust_Data[0].customer_log[0].father_name}` , margin: [ 0, 0, 0,5 ] },

                                                    ]
                                                ]
                                            },
                                            {
                                                columns: [
                                                    {text: 'Occupation:', style: 'heading'} ,
                                                    [
                                                        { text:`${service_data[0].name}` , margin: [ 0, 0, 0,5 ] },

                                                    ]
                                                ],
                                            },
                                            {
                                                columns: [
                                                    {text: 'Nature of Profession:', style: 'heading'} ,
                                                    [
                                                        { text:`${cust_Data[0].customer_log[0].ocupation.nature_of_profession}`, margin: [ 0, 0, 0,5 ]  },

                                                    ],

                                                ]
                                            },
                                            {
                                                columns: [
                                                    {
                                                        text: 'Organization/Institution Name(Optional):',
                                                        style: 'heading'
                                                    },
                                                    [
                                                        {text: `${cust_Data[0].customer_log[0].organization_institution_name}` , margin: [ 0, 0, 0,5 ]},

                                                    ],

                                                ],
                                            },
                                            {
                                                columns: [
                                                    {text: 'Customer/Representative Designation:', style: 'heading'} ,
                                                    [
                                                        { text:`${cust_Data[0].customer_log[0].organization_Designation}` , margin: [ 0, 0, 0,5 ] },

                                                    ]
                                                ]
                                            },
                                            {
                                                columns: [
                                                    {text: 'Customer/Representative Organization/Institude Name', style: 'heading'} ,
                                                    [
                                                        { text:`${cust_Data[0].customer_log[0].organization_institution_name}` , margin: [ 0, 0, 0,5 ] },

                                                    ]
                                                ]
                                            },
                                            {
                                                columns: [
                                                    {text: 'Customer/Representative Organization/Institude Address', style: 'heading'} ,
                                                    [
                                                        { text:`${cust_Data[0].customer_log[0].organization_address}` , margin: [ 0, 0, 0,5 ] },

                                                    ]
                                                ]
                                            },
                                            {
                                                columns: [
                                                    {text: 'CNIC:', style: 'heading'} ,
                                                    [
                                                        { text:`${cust_Data[0].customer_log[0].national_identity}` , margin: [ 0, 0, 0,5 ] },

                                                    ],


                                                ]
                                            },
                                            {
                                                columns: [
                                                    {text: 'Contact:', style: 'heading'} ,
                                                    [
                                                        { text:`${cust_Data[0].customer_log[0].telephone}` , margin: [ 0, 0, 0,5 ] },

                                                    ],
                                                ]
                                            },
                                            {
                                                columns: [
                                                    {text: 'NTN:', style: 'heading'} ,
                                                    [
                                                        { text:`${cust_Data[0].customer_log[0].ntn}` , margin: [ 0, 0, 0,5 ] },

                                                    ]

                                                ]
                                            },
                                            {
                                                columns: [
                                                    {text: 'Nature of Transaction:', style: 'heading'} ,
                                                    [
                                                        { text:`${edd_Data[0].nature_of_transaction}` , margin: [ 0, 0, 0,5 ] },

                                                    ],

                                                ]
                                            },
                                            {
                                                columns: [
                                                    {text: 'Beneficiary Name:', style: 'heading'} ,
                                                    [
                                                        { text:`${cust_Data[0].customer_log[0].beneficiary_detail.name}` , margin: [ 0, 0, 0,5 ] },

                                                    ],
                                                    {text: 'Relationship with Beneficiary:', style: 'heading'} ,
                                                    [
                                                        { text:`${cust_Data[0].customer_log[0].beneficiary_detail.relationship}` , margin: [ 0, 0, 0,5 ] },

                                                    ],

                                                ]
                                            },
                                            {text: 'In Case Of Beneficial Owner Transaction', style: 'heading'},
                                            {
                                                columns: [
                                                    {text: 'Actual Beneficial Owner Name:', style: 'heading'} ,
                                                    [
                                                        { text:`${cust_Data[0].customer_log[0].authorized_person.name}` , margin: [ 0, 0, 0,5 ]},

                                                    ],

                                                ]
                                            },
                                            {
                                                columns: [
                                                    {text: 'Relationship With Actual Benefical Owner:', style: 'heading'} ,
                                                    [
                                                        { text:`${cust_Data[0].customer_log[0].authorized_person.relation}` , margin: [ 0, 0, 0,5 ]},

                                                    ],

                                                ]
                                            },
                                            {
                                                columns: [
                                                    {text: 'Beneficial Owner CNIC:', style: 'heading'} ,
                                                    [
                                                        { text:`${cust_Data[0].customer_log[0].authorized_person.national_id}` , margin: [ 0, 0, 0,5 ]},

                                                    ],

                                                ]
                                            },
                                            {
                                                columns: [
                                                    {text: 'Beneficial Owner Contact Number:', style: 'heading'} ,
                                                    [
                                                        { text:`${cust_Data[0].customer_log[0].authorized_person.beneficial_cell_number}` , margin: [ 0, 0, 0,5 ]},

                                                    ],

                                                ]
                                            },
                                            {
                                                columns: [
                                                    {text: 'Actual Beneficial Owner Occupation:', style: 'heading'} ,
                                                    [
                                                        { text:`${cust_Data[0].customer_log[0].authorized_person.beneficial_owner_occupation}` , margin: [ 0, 0, 0,5 ]},

                                                    ],

                                                ]
                                            },
                                            {
                                                columns: [
                                                    {text: 'Actual Beneficial Owner Call Confirmation:', style: 'heading'} ,
                                                    [
                                                        { text:`${cust_Data[0].customer_log[0].authorized_person.beneficial_owner_cell_confirmation}` , margin: [ 0, 0, 0,5 ]},

                                                    ],

                                                ]
                                            },
                                            {
                                                columns: [
                                                    {text: 'Actual Beneficial Owner NTN (If Any):', style: 'heading'} ,
                                                    [
                                                        { text:`${cust_Data[0].customer_log[0].authorized_person.beneficial_owner_ntn}` , margin: [ 0, 0, 0,5 ]},

                                                    ],

                                                ]
                                            },
                                            {
                                                columns: [
                                                    {text: 'Actual Beneficial Owner Designation:', style: 'heading'} ,
                                                    [
                                                        { text:`${cust_Data[0].customer_log[0].authorized_person.beneficial_owner_designation}` , margin: [ 0, 0, 0,5 ]},

                                                    ],

                                                ]
                                            },
                                            {
                                                columns: [
                                                    {text: 'Authority Letter Obtained:', style: 'heading'} ,
                                                    [
                                                        { text:`${cust_Data[0].customer_log[0].authorized_person.is_authorized}` , margin: [ 0, 0, 0,5 ]},

                                                    ],

                                                ]
                                            },

                                            {
                                                columns: [
                                                    {text: 'Total Amount in(Figures in Rs.):', style: 'heading'} ,
                                                    [
                                                        { text:`${edd_Data[0].amount_in_figure}` , margin: [ 0, 0, 0,5 ] },

                                                    ],

                                                ]
                                            },
                                            {
                                                columns: [
                                                    {text: 'Total Amount in Words:', style: 'heading'} ,
                                                    [
                                                        { text:`${edd_Data[0].amount_in_words}` , margin: [ 0, 0, 0,5 ] },

                                                    ],

                                                ]
                                            },
                                            {
                                                columns: [
                                                    {text: 'Customer Type:', style: 'heading'} ,
                                                    [
                                                        { text:`${customer_type_data[0].name}`, margin: [ 0, 0, 0,5 ]  },

                                                    ],

                                                ]
                                            },
                                            {
                                                columns: [
                                                    {text: 'Reason of EDD:', style: 'heading'} ,
                                                    [
                                                        { text:`${reasondata}` , margin: [ 0, 0, 0,5 ] },

                                                    ],

                                                ]
                                            },
                                            {
                                                columns: [
                                                    {text: 'In Case Of Transaction with PEP:', style: 'heading'},
                                                    [
                                                        {text: `${pep_data ? pep_data[0].name : ""}` , margin: [ 0, 0, 0,5 ]},

                                                    ],

                                                ]
                                            },
                                            {
                                                columns: [
                                                    {text: 'Associates/Family member(PEP):', style: 'heading'},
                                                    [
                                                        {text: `${cust_Data[0].customer_log[0].ocupation.associates}` , margin: [ 0, 0, 0,5 ]},

                                                    ],

                                                ],
                                            },
                                            {
                                                columns: [
                                                    {text: 'Any Other(Specify):', style: 'heading'} ,
                                                    [
                                                        { text:`${cust_Data[0].customer_log[0].ocupation.others}` , margin: [ 0, 0, 0,5 ] },

                                                    ],

                                                ]
                                            },


                                            {text: 'IN CASE OF THIRD PARTY TRANSACTIONS', style: 'header'},
                                            {text: 'For TT OUTWARD Transactions', style: 'heading'},
                                            {
                                                columns: [
                                                    {text: 'Customer Relationship with Receiver:', style: 'heading'} ,
                                                    [
                                                        { text:`${cust_Data[0].customer_log[0].relation_with_receiver.relation}` , margin: [ 0, 0, 0,5 ]},

                                                    ],

                                                ]
                                            },
                                            {
                                                columns: [
                                                    {text: 'Receiver Occupation:', style: 'heading'} ,
                                                    [
                                                        { text:`${cust_Data[0].customer_log[0].relation_with_receiver.occupation}` , margin: [ 0, 0, 0,5 ]},

                                                    ],

                                                ]
                                            },
                                            {
                                                columns: [
                                                    {text: 'Source of Fund:', style: 'heading'} ,
                                                    [
                                                        { text:`${fund_source_data[0].name}` , margin: [ 0, 0, 0,5 ]},

                                                    ],
                                                    {text: 'Any Other(Specify):', style: 'heading'} ,
                                                    [
                                                        { text:`${cust_Data[0].customer_log[0].source_of_fund.others}` , margin: [ 0, 0, 0,5 ]},

                                                    ],

                                                ]
                                            },
                                            {text: 'No. of Transaction in the previous month (for Branch Officical)', style: 'heading'},
                                            {
                                                columns: [
                                                    {text:  `In ${edd_Data[0].previous_transactions.type} Month:`, style: 'heading'} ,

                                                    [
                                                        { text: `${edd_Data[0].previous_transactions.value}` , margin: [ 0, 0, 0,5 ]},

                                                    ],
                                                ]
                                            },
                                            {text: 'Brief Customer Profile:[Explain why He/She shouldn\'t be reported as Suspicious (STR)]',  style: 'heading' , margin: [ 0, 0, 0,5 ]},
                                            {
                                                columns: [

                                                    [
                                                        { text:` ${edd_Data[0].customer_feedback} ` , margin: [ 0, 0, 0,5 ]},

                                                    ],

                                                ]
                                            },
                                            {text: 'STATUS',  style: 'heading' , margin: [ 0, 0, 0,0 ]},
                                            {
                                                columns: [

                                                    [
                                                        { text:` ${edd_Data[0].status ==  2 ? 'Rejected' : edd_Data[0].status ==  1 ? 'Approved' : edd_Data[0].status ==  0 ? 'Pending' : 'N/A'  } ` , margin: [ 0, 0, 0,0 ]},

                                                    ],

                                                ]
                                            },

                                        ],
                                        styles: {
                                            heading: {
                                                fontSize: 12,
                                                bold: true
                                            },
                                            header: {
                                                fontSize: 14,
                                                bold: true
                                            },


                                        }
                                    };

                                    const pdfDoc = pdfMake.createPdf(documentDefinition);
                                    pdfDoc.getBase64((data)=>{
                                        res.writeHead(200,
                                            {
                                                'Content-Type': 'application/pdf',
                                                'Content-Disposition':'attachment;filename="Edd.pdf"'
                                            });

                                        const download = Buffer.from(data.toString('utf-8'), 'base64');
                                        res.end(download);
                                    });
                                })
                            })

                        })
                    })
                })
            })
        })
    }catch (e) {
        redirect('/admin/edd');
    }
})



/* Add EEd Customer. */
router.get('/add', function (req, res, next) {
    // Region.find().where({is_deleted: false}).lean().exec((err, doc) => {

    customer_type.find({}, function (err, customertype) {
        service_type.find({}, function (err, services) {
            pep_type.find({}, function (err, pep) {
                fund_source.find({}, function (err, sources) {
                    reason_edd.find({}, function (err, reasonedd) {

                        res.render('admin/edd/form', {
                            layout: 'admin/layouts/default',
                            users: req.user.role_id,
                            username:req.user.username,
                            Customers_type: customertype,
                            services_type: services,
                            pep_type: pep,
                            source_of_fund: sources,
                            reasons_edd: reasonedd
                        })
                        // });
                    });
                });
            });
        });
    });
});

/* POST EEd Customer. */
router.post('/add', async function (req, res, next) {
    data = req.body;

    var valid = validation.validate(data, {});
    if (Object.keys(valid).length > 0) {
        error_message = "";
        for (var kv in valid) {
            for (var v in valid[kv]) {
                error_message += valid[kv][v] + "<br/>"
            }
        }
        req.flash('error', error_message);
        res.redirect('add');
    } else {
        var cust = {

            customer_name: data.customer_name,
            father_name: data.father_name,
            national_identity: data.national_identity,
            telephone: data.telephone,
            ntn: data.ntn,
            beneficiary_detail: {
                name: data.Beneficiary_name,
                relationship: data.relationship_with_beneficiary,
            },
            customer_type: data.customer_type,
            ocupation: {
                type_of_service: data.type_of_service,
                nature_of_profession: data.nature_of_profession,
                organization_name: data.organization_name,
                pep_type: data.pep_type,
                associates: data.associates,
                others: data.others,

            },
            relation_with_receiver: {
                relation: data.relation,
                occupation: data.occupation,
            },
            authorized_person: {
                name: data.authorized_person_name,
                relation: data.authorized_person_relation,
                national_id: data.authorized_person_national_id,
                is_authorized: data.is_authorized,

            },
            source_of_fund: {
                source: data.source_of_fund_source,
                others: data.source_of_fund_others,
            },

            // region_id: helper.decrypt(data.region),
        };
        var customer = new customers({
            national_identity: data.national_identity,
            customer_log: cust,
        });
        var user_id = req.user.id;
        var edd_json = {
            user_id: user_id,
            customer_id: customer._id,
            form_date: data.form_date,
            voucher_no: data.voucher_no,
            nature_of_transaction: data.nature_of_transaction,
            amount_in_figure: data.amount_in_figure,
            amount_in_words: data.amount_in_words,
            reason_of_edd: data.reason_of_edd,
            previous_transactions: {
                type: data.previous_transactions_type,
                value: parseInt(data.previous_transactions_value),
            },
            additional_documents: data.additional_documents,
            customer_feedback: data.customer_feedback,
            // region_id: helper.decrypt(data.region),
        };
        reporting_line.find({}, function (error, data) {

            // data.forEach(function(fetch){
            // console.log(data[0].department_id);

            User.find({department: data[0].department_id}, function (e, d) {
                edd_json.approval = [];
                for (ed in d) {
                    //console.log(d[ed]._id);
                    edd_json.approval.push({
                        userid: d[ed]._id,
                        status: 0
                    });
                }
                var edd = new Edd(edd_json);
                customer.save().then((customerDoc) => {

                    edd.save().then((eddDoc) => {
                        req.flash('info', "Successfully added.");
                        res.redirect('/admin/edd');
                        console.log('>>>');
                    }, (e) => {
                        req.flash('error', e);
                        res.redirect('edd/add');
                        console.log(e);
                    });
                });
            });

            //  })


        }).sort({"sort": 1});


        // dept.forEach(async function(data){

        //  var u = await user.find({department:data._id});
        //  console.log(u);
        //  })


    }
})

/* GET EEd Customer. */

router.get('/:id?', function (req, res, next) {

    var perPage = 10;
    var page = req.param('page') || 1;
    id = req.params.id;
    where = {
        is_deleted: false,
    }
    if (id != undefined) {
        id = helper.decrypt(id);
        where._id = id;
    }


    Edd.find({}).sort({'date_added': 'desc'})
        .populate('customer_id')
        .skip((perPage * page) - perPage)
        .limit(perPage)
        .where(where)
        .lean()
        .exec((err, doc) => {

            if (err) {
                req.flash('error', "Unable to process your request.");
                res.redirect('/admin/edd');
            }
            else {
                Edd.count().where(where).exec(function (err, count) {
                    for (index in doc) {
                        doc[index].customer_name = "";
                        if (doc[index].customer_id != null) {
                            doc[index].customer_name = doc[index].customer_id.customer_log[0].customer_name;
                            doc[index].national_identity = doc[index].customer_id.customer_log[0].national_identity;
                        }
                    }
                    for (edds_index in doc) {
                        for (approval_index in doc[edds_index].approval) {
                            if (doc[edds_index].approval[approval_index].userid == req.user.id) {
                                doc[edds_index].approval_status = doc[edds_index].approval[approval_index].status;
                                doc[edds_index].is_rejected = doc[edds_index].approval[approval_index].is_rejected;

                            }
                        }
                    }
                    res.render('admin/edd/index', {
                        layout: 'admin/layouts/default', users: req.user.role_id,username:req.user.username, moment: moment,
                        institutes: helper.manipulate(doc,
                            ['_id'],
                            ['is_active', 'is_deleted']
                        ),
                        current: page,
                        pages: Math.ceil(count / perPage)

                    });
                });
            }
        });

});


/* GET View Details Edd Customer. */
router.get('/view/:id', function (req, res, next) {


    try {


        id = helper.decrypt(req.params.id);

        if (!id || id === null || id.length === undefined) {
            req.flash('error', "Unable to process your request.");
            res.redirect('/admin/edd');
        } else {
            Edd.findById(id, function (err, data) {
                customers.findById(data.customer_id, function (err, customer_data) {
                    customer_type.findById(customer_data.customer_log[0].customer_type, function (err, customer_type_data) {
                        service_type.findById(customer_data.customer_log[0].ocupation.type_of_service, function (err, customer_service_type) {
                            pep_type.findById(customer_data.customer_log[0].ocupation.pep_type, function (err, customer_pep_type) {
                                fund_source.findById(customer_data.customer_log[0].source_of_fund.source, function (err, customer_source_of_fund) {
                                    // _.map(data.reason_of_edd,(item, index)=>{

                                    //     reason_edd.findById({_id:item},function(err,reason_edd_data){
                                    //     });
                                    // });
                                    reason_edd.find({
                                        '_id': {
                                            $in: data.reason_of_edd
                                        }
                                    }, function (err, reason) {
                                        if (!data) {
                                            req.flash('error', "Unable to process your request.");
                                            res.redirect('/admin/edd');
                                        } else {
                                            res.render('admin/edd/view', {
                                                layout: 'admin/layouts/default',
                                                moment: moment,
                                                customer: customer_data.customer_log[0],
                                                users: req.user.role_id,
                                                username:req.user.username,
                                                edd: data,
                                                customer_type: customer_type_data,
                                                customer_service_types: customer_service_type,
                                                customer_pep_types: customer_pep_type,
                                                customer_source_of_funds: customer_source_of_fund,
                                                reason_of_edd: reason

                                            })
                                        }
                                    })


                                });
                            });
                        });

                    });
                });
            });
        }
    } catch (e) {
        redirect('/admin/edd');
    }
});

/* GET Edit. */
router.get('/edit/:id', function (req, res, next) {
    try {


        id = helper.decrypt(req.params.id);
        if (!id || id === null || id.length === undefined) {
            req.flash('error', "Unable to process your request.");
            res.redirect('/admin/edd');
        } else {
            Edd.findById(id, function (err, data) {

                customers.findById(data.customer_id, function (err, customer_data) {
                    customer_type.find({is_deleted:false}, function (err, customer_type_data) {
                        service_type.find({is_deleted:false}, function (err, customer_service_type) {
                            pep_type.find({is_deleted:false}, function (err, customer_pep_type) {
                                fund_source.find({is_deleted:false}, function (err, customer_source_of_fund) {
                                    reason_edd.find({is_deleted:false}, function (err, reasonedd) {

                                        // _.map(data.reason_of_edd,(item, index)=>{

                                        //     reason_edd.findById({_id:item},function(err,reason_edd_data){
                                        //     });
                                        // });


                                        if (!data) {
                                            req.flash('error', "Unable to process your request.");
                                            res.redirect('/admin/edd');
                                        } else {
                                            res.render('admin/edd/edit', {
                                                layout: 'admin/layouts/default',
                                                moment: moment,
                                                customer: customer_data.customer_log[0],
                                                users: req.user.role_id,
                                                username:req.user.username,
                                                edd: data,
                                                customer_type: customer_type_data,
                                                customer_service_types: customer_service_type,
                                                customer_pep_types: customer_pep_type,
                                                customer_source_of_funds: customer_source_of_fund,
                                                reason_edd: reasonedd,
                                                id: helper.encrypt(data._id.toString())
                                            })
                                        }
                                    });
                                });
                            });
                        });

                    });
                });
            });
        }
    }
    catch (e) {
        redirect('/admin/edd');
    }
});

router.post("/edit/:id", uploads.array('additional_documents',10), function (req, res, next) {
    try {
        id = helper.decrypt(req.params.id);

        data = req.body;

        if (!id || id === null || id.length === undefined) {
            req.flash('error', "Unable to process your request.");
            res.redirect('/admin/edd');
        } else {
            Edd.findById(id, function (err, d) {

                if (!d) {
                    req.flash('error', "Unable to process your request.");
                    res.redirect('/admin/edd');
                } else {

                    if (req.files == "") {
                        var documents = [];
                        for (files_index in data.additional_document_old) {
                            console.log(files_index);
                            documents.push({
                                name: data.additional_document_name[files_index],
                                document: data.additional_document_old
                            });

                        }
                    }
                    else {

                        var documents = [];
                        for (file in req.files) {
                            documents.push({

                                name: data.additional_document_name[file],
                                document: req.files[file].filename
                            });

                        }
                    }




                    if (data.form_date == "") {
                        d.form_date = data.form_date_old;
                    }
                    else {
                        d.form_date = data.form_date;
                    }

                    d.voucher_no = data.voucher_no;
                    d.amount_in_figure = data.amount_in_figure;
                    d.amount_in_words = data.amount_in_words;
                    d.customer_feedback = data.customer_feedback;
                    d.additional_documents = documents;

                    if (data.reason_of_edd == undefined) {
                        d.reason_of_edd =  d.reason_of_edd;
                    }
                    else {
                        d.reason_of_edd = data.reason_of_edd;
                    }
                    d.nature_of_transaction = data.nature_of_transaction;

                    d.date_update = new Date();
                    previous_transactions :  {
                        d.previous_transactions.type = data.previous_transactions_type,
                            d.previous_transactions.value = data.previous_transactions_value
                    }
                    customers.findById(d.customer_id, function (err, cust_Data) {

                        if (!d) {
                            req.flash('error', "Unable to process your request.");
                            res.redirect('/admin/edd');
                        }

                        else {
                            cust_Data.customer_log[0].customer_name = data.customer_name;
                            cust_Data.customer_log[0].father_name = data.father_name;
                            cust_Data.customer_log[0].national_identity = data.national_identity;
                            cust_Data.customer_log[0].telephone = data.telephone;
                            cust_Data.customer_log[0].ntn = data.ntn;
                            cust_Data.customer_log[0].customer_type = data.customer_type;
                            cust_Data.customer_log[0].customer_type_non_resident_location = data.customer_type_location;
                            cust_Data.customer_log[0].customer_type_foreginer_nationality = data.customer_type_nationality;

                            cust_Data.customer_log[0].date_update = new Date();
                            source_of_fund: {
                                cust_Data.customer_log[0].source_of_fund.source = data.source_of_fund_source,
                                    cust_Data.customer_log[0].source_of_fund.others = data.source_of_fund_others

                            }
                            authorized_person: {
                                cust_Data.customer_log[0].authorized_person.name = data.beneficial_owner_name,
                                    cust_Data.customer_log[0].authorized_person.relation = data.beneficial_owner_relation,
                                    cust_Data.customer_log[0].authorized_person.national_id = data.beneficial_owner_cnic,
                                    cust_Data.customer_log[0].authorized_person.beneficial_cell_number = data.beneficial_owner_cell_number,
                                    cust_Data.customer_log[0].authorized_person.beneficial_owner_occupation = data.beneficial_owner_occcupation,
                                    cust_Data.customer_log[0].authorized_person.beneficial_owner_Organization = data.beneficial_owner_Organization,
                                    cust_Data.customer_log[0].authorized_person.beneficial_owner_cell_confirmation = data.beneficial_owner_cell_confirmation,
                                    cust_Data.customer_log[0].authorized_person.beneficial_owner_ntn = data.beneficial_owner_ntn,
                                    cust_Data.customer_log[0].authorized_person.beneficial_owner_designation = data.beneficial_owner_designation,
                                    cust_Data.customer_log[0].authorized_person.is_authorized = data.is_authorized
                            }
                            relation_with_receiver: {
                                cust_Data.customer_log[0].relation_with_receiver.relation = data.relation,
                                    cust_Data.customer_log[0].relation_with_receiver.occupation = data.occupation

                            }
                            ocupation: {
                                cust_Data.customer_log[0].ocupation.type_of_service = data.type_of_service,
                                    cust_Data.customer_log[0].ocupation.Other = data.service_other_specify,
                                    cust_Data.customer_log[0].ocupation.nature_of_profession = data.nature_of_profession,
                                    cust_Data.customer_log[0].ocupation.organization_name = data.organization_name,
                                    cust_Data.customer_log[0].ocupation.pep_type = data.pep_type,
                                    cust_Data.customer_log[0].ocupation.pep_associates = data.pep_associates,
                                    cust_Data.customer_log[0].ocupation.pep_other = data.pep_other
                            }
                            beneficiary_detail: {
                                cust_Data.customer_log[0].beneficiary_detail.name = data.beneficiary_name,
                                    cust_Data.customer_log[0].beneficiary_detail.relationship = data.relationship_with_beneficiary

                            }
                            cust_Data.national_identity = data.national_identity;


                        }


                        // d.region_id = helper.decrypt(data.region);
                        d.save(function (err) {
                            cust_Data.save(function (err) {
                                if (err) {
                                    req.flash('error', "Unable to process your request.");
                                    res.redirect('/admin/edd');
                                } else {
                                    req.flash('info', "Successfully updated.");
                                    res.redirect('/admin/edd');
                                }
                            })
                        })
                    });
                }

            });
        }
    }catch (e) {
        redirect('/admin/edd');
    }
})

/* GET Remove. */
router.get('/remove/:id', function (req, res, next) {
    try {


        id = helper.decrypt(req.params.id);
        if (!id || id === null || id.length === undefined) {
            Edd.findById(id, function (err, data) {
                if (!data) {
                    req.flash('error', "Unable to process your request.");
                    res.redirect('/admin/edd');
                } else {
                    data.is_deleted = true;
                    data.save(function (err) {
                        if (err) {
                            req.flash('error', "Unable to process your request.");
                            res.redirect('/admin/edd');
                        } else {
                            req.flash('info', "Successfully deleted.");
                            res.redirect('/admin/edd');
                        }
                    })
                }
            });
        }
    }catch (e) {
        redirect('/admin/edd');
    }
});

router.get('/approval_log/:id', function (req, res) {
    try {


        edd_id = helper.decrypt(req.params.id);
        if (!edd_id || edd_id === null || edd_id.length === undefined) {

            req.flash('error', "Unable to process your request.");
            res.redirect('/admin/edd');
        }
        else {

            Edd.find({_id: edd_id}, function (err, edd_data) {

                getdata = [];
                for (index in edd_data) {
                    for (approval_index in edd_data[index].approval) {
                        getdata.push(edd_data[index].approval[approval_index].userid)
                    }
                }
                User.find({
                    _id: {
                        $in: getdata
                    }
                }, function (err, udata) {
                    for (uindex in udata) {
                        for (approval_index in edd_data[0].approval) {
                            if (edd_data[0].approval[approval_index].userid == udata[uindex]._id) {
                                udata[uindex].edd_status = edd_data[0].approval[approval_index].status
                                udata[uindex].edd_rejected = edd_data[0].approval[approval_index].is_rejected
                                udata[uindex].edd_rejected_reason = edd_data[0].approval[approval_index].rejected_reason

                            }
                        }
                    }
                    res.render('admin/edd/approval_log', {
                        layout: 'admin/layouts/default', users: req.user.role_id,username:req.user.username,
                        approval_log: udata,
                        edd_data: edd_data,
                        usrdata: udata,


                    })

                })
            })
        }
    } catch (e) {
        redirect('/admin/edd');
    }
});

module.exports = router;