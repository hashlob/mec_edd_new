var express = require('express');
var router = express.Router();
var Institute = require('../../models/admin/fund_source');
var helper = require('../../helpers/general');
var validation = require('../../helpers/validation');

//multer object creation
var multer = require('multer')
var storage = multer.diskStorage({
    destination: function (req, file, cb) {
        cb(null, 'public/uploads/')
    },
    filename: function (req, file, cb) {
        var extension = file.originalname.split('.');
        cb(null, Date.now() + "." + extension[1])
    }
})

var upload = multer({storage: storage})

/* GET Add. */
router.get('/add', function (req, res, next) {
    // Region.find().where({is_deleted: false}).lean().exec((err, doc) => {
        res.render('admin/fund_source/form', {
            layout: 'admin/layouts/default',users:req.user.role_id,username:req.user.username,
        })
    // });
});

/* POST Add. */
router.post('/add', function (req, res, next) {
    data = req.body;
    var valid = validation.validate(data, {
        "name": {
            required: true
        },
    });
    if (Object.keys(valid).length > 0) {
        error_message = "";
        for (var kv in valid) {
            for (var v in valid[kv]) {
                error_message += valid[kv][v] + "<br/>"
            }
        }
        req.flash('error', error_message);
        res.redirect('add');
    } else {
        var unit = new Institute({
            name: data.name,
           // region_id: helper.decrypt(data.region),
           
        });
        unit.save().then((doc) => {
            req.flash('info', "Successfully added.");
            res.redirect('/admin/fund_source');
        }, (e) => {
            req.flash('error', e);
            res.redirect('fund_source/add');
        });
    }
})

/* GET Index. */
router.get('/:id?', function (req, res, next) {
    var perPage = 10;
    var page = req.param('page') || 1;
    id = req.params.id;
    where = {
        is_deleted: false,
    }
    if (id != undefined) {
        id = helper.decrypt(id);
        where._id = id;
    }
    Institute.find().sort({'date_added': 'desc'})
        .skip((perPage * page) - perPage)
        .limit(perPage)
        .where(where)
        .lean()
        .exec((err, doc) => {
            if (err) {
                req.flash('error', "Unable to process your request.");
                res.redirect('/admin/fund_source');
            } else {
                Institute.count().where(where).exec(function (err, count) {
                    res.render('admin/fund_source/index', {
                        layout: 'admin/layouts/default',users:req.user.role_id,username:req.user.username,
                        institutes: helper.manipulate(doc,
                            ['_id'],
                            ['is_active', 'is_deleted']
                        ),
                        current: page,
                        pages: Math.ceil(count / perPage)
                    });
                });
            }
        });

});

/* GET Remove. */
router.post('/remove/:id', function (req, res, next) {
    try {


    id = helper.decrypt(req.params.id);
        if (id) {
        Institute.findById(id, function (err, data) {
            if (!data) {
                res.send({
                    status: false,
                    message: "Unable to process your request."
                })
            } else {
                data.is_deleted = true;
                data.save(function (err) {
                    if (err) {
                        res.send({
                            status: false,
                            message: "Unable to process your request."
                        })
                    } else {
                        res.send({
                            status: true,
                            message: "Successfully Delete"
                        });
                    }
                })
            }
        });
    } }
    catch (e) {
        redirect('/admin/fund_source');
    }
});

/* GET View Details. */
router.get('/view/:id', function (req, res, next) {
    try {


    id = helper.decrypt(req.params.id);

        if (!id || id === null || id.length === undefined) {
            req.flash('error', "Unable to process your request.");
            res.redirect('/admin/fund_source');
        }
        else {
            Institute.findById(id, function (err, data) {
                if (!data) {
                    req.flash('error', "Unable to process your request.");
                    res.redirect('/admin/fund_source');
                } else {
                    res.render('admin/fund_source/view', {
                        layout: 'admin/layouts/default', users: req.user.role_id,username:req.user.username,
                        institute: data
                    })
                }
            });

    }
    }
    catch (e) {
        redirect('/admin/fund_source');
    }

});

/* GET Edit. */
router.get('/edit/:id', function (req, res, next) {
    try {


        id = helper.decrypt(req.params.id);
        if (!id || id === null || id.length === undefined) {
            req.flash('error', "Unable to process your request.");
            res.redirect('/admin/fund_source');
        } else {
            Institute.findById(id, function (err, data) {

                res.render('admin/fund_source/edit', {
                    layout: 'admin/layouts/default', users: req.user.role_id,username:req.user.username,

                    institute: data,
                    helper: helper,
                    id: helper.encrypt(data._id.toString())
                })

            })
        }
    }
    catch (e) {
        redirect('/admin/fund_source');
    }
});

router.post("/edit/:id", function(req, res, next){
    try {


        id = helper.decrypt(req.params.id);
        data = req.body;
        console.log(data);
        if (!id || id === null || id.length === undefined) {
            req.flash('error', "Unable to process your request.");
            res.redirect('/admin/fund_source');
        } else {
            Institute.findById(id, function (err, d) {
                if (!d) {
                    req.flash('error', "Unable to process your request.");
                    res.redirect('/admin/fund_source');
                } else {
                    d.name = data.name;
                    // d.region_id = helper.decrypt(data.region);
                    d.save(function (err) {
                        if (err) {
                            req.flash('error', "Unable to process your request.");
                            res.redirect('/admin/fund_source');
                        } else {
                            req.flash('info', "Successfully updated.");
                            res.redirect('/admin/fund_source');
                        }
                    })
                }
            });
        }
    }
    catch (e) {
        redirect('/admin/fund_source');
    }
})


module.exports = router;