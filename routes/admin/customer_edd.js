var express = require('express');
const mongoose = require('mongoose');
var router = express.Router();
var Edd = require('../../models/admin/edd');
var Unit = require('../../models/admin/unit');
var department = require('../../models/admin/department');
var customers = require('../../models/admin/customer');
var customer_type = require('../../models/admin/customer_type');
var reason_edd = require('../../models/admin/reason_edd');
var service_type = require('../../models/admin/service_type');
var pep_type = require('../../models/admin/pep_type');
var fund_source = require('../../models/admin/fund_source');
var User = require('../../models/admin/user');
var reporting_line = require('../../models/admin/reporting_line');
var helper = require('../../helpers/general');
var validation = require('../../helpers/validation');
var _ = require('lodash');
var moment = require('moment');
//multer object creation
var multer = require('multer');

var crypto = require('crypto');
var mime = require('mime');

var storage = multer.diskStorage({
    destination: function (req, file, cb) {
        cb(null, './uploads/')
    },
    filename: function (req, file, cb) {
        crypto.pseudoRandomBytes(16, function (err, raw) {
            cb(null, raw.toString('hex') + Date.now() + '.' + mime.getExtension(file.mimetype));
        });
    }
});

var upload = multer({storage: storage});

var storage_additional_document = multer.diskStorage({
    destination: function (req, file, cb) {
        cb(null, './additional_document/')
    },
    filename: function (req, file, cb) {
        crypto.pseudoRandomBytes(16, function (err, raw) {
            cb(null, raw.toString('hex') + Date.now() + '.' + mime.getExtension(file.mimetype));
        });
    }
});

var uploads = multer({storage: storage_additional_document});

/* Add EEd Customer. */
router.get('/add', function (req, res, next) {
    // Region.find().where({is_deleted: false}).lean().exec((err, doc) => {

    customer_type.find({is_deleted:false}, function (err, customertype) {
        service_type.find({is_deleted:false}, function (err, services) {
            pep_type.find({is_deleted:false}, function (err, pep) {
                fund_source.find({is_deleted:false}, function (err, sources) {
                    reason_edd.find({is_deleted:false}, function (err, reasonedd) {
                        department.find({_id:req.user.department},function (err,dept_Data) {

                            Unit.find({_id:dept_Data[0].unit_id},function (err,unit_Data) {


                                res.render('admin/customer_edd/form', {
                                    layout: 'admin/layouts/default',
                                    users: req.user.role_id,
                                    username:req.user.username,
                                    Customers_type: customertype,
                                    services_type: services,
                                    pep_type: pep,
                                    source_of_fund: sources,
                                    reasons_edd: reasonedd,
                                    unit_Data:unit_Data[0],
                                })
                                // });
                            });
                        });
                    });
                });
            });
        });
    })
})
/* POST EEd Customer. */
router.post('/add', uploads.array('additional_documents', 12), function (req, res, next) {
    data = req.body;

    var valid = validation.validate(data, {});
    if (Object.keys(valid).length > 0) {
        error_message = "";
        for (var kv in valid) {
            for (var v in valid[kv]) {
                error_message += valid[kv][v] + "<br/>"
            }
        }
        req.flash('error', error_message);
        res.redirect('add');
    } else {
        var cust = {

            customer_name: data.customer_name,
            father_name: data.father_name,
            national_identity: data.national_identity,
            telephone: data.telephone,
            ntn: data.ntn,
            organization_Designation: data.organization_Designation,
            organization_institution_name: data.organization_institution_name,
            organization_address: data.organization_address,
            beneficiary_detail: {
                name: data.Beneficiary_name,
                relationship: data.relationship_with_beneficiary,
            },
            customer_type: data.customer_type,
            customer_type_non_resident_location:data.customer_type_location,
            customer_type_foreginer_nationality:data.customer_type_nationality,
            ocupation: {
                type_of_service: data.type_of_service,
                Other: data.service_other_specify,
                nature_of_profession: data.nature_of_profession,
                organization_name: data.organization_name,
                pep_type: data.pep_type,
                pep_associates: data.pep_associates,
                pep_other: data.pep_other,

            },
            relation_with_receiver: {
                relation: data.relation,
                occupation: data.occupation,
            },
            authorized_person: {
                name: data.beneficial_owner_name,
                relation: data.beneficial_owner_relation,
                national_id: data.beneficial_owner_cnic,
                beneficial_cell_number:data.beneficial_owner_cell_number,
                beneficial_owner_occupation:data.beneficial_owner_occcupation,
                beneficial_owner_Organization:data.beneficial_owner_Organization,
                beneficial_owner_Organization_address:data.beneficial_owner_Organization_address,
                beneficial_owner_cell_confirmation:data.beneficial_owner_cell_confirmation,
                beneficial_owner_ntn:data.beneficial_owner_ntn,
                beneficial_owner_designation:data.beneficial_owner_designation,
                is_authorized: data.is_authorized,

            },
            source_of_fund: {
                source: data.source_of_fund_source,
                others: data.source_of_fund_others,
            },

            // region_id: helper.decrypt(data.region),
        };
        var customer = new customers({
            national_identity: data.national_identity,
            customer_log: cust,
        });
        var user_id = req.user.id;

        var documents = [];
        for (file in req.files) {
            documents.push({
                name: data.additional_document_name[file],
                document: req.files[file].filename
            });
        }
        var document = documents;

        var edd_json = {
            branch_name:data.branch_name,
            user_id: user_id,
            customer_id: customer._id,
            form_date: data.form_date,
            voucher_no: data.voucher_no,
            nature_of_transaction: data.nature_of_transaction,
            amount_in_figure: data.amount_in_figure,
            amount_in_words: data.amount_in_words,
            reason_of_edd: data.reason_of_edd,

            previous_transactions: {
                type: data.previous_transactions_type,
                value: parseInt(data.previous_transactions_value),
            },

            additional_documents: document,


            customer_feedback: data.customer_feedback,
            // region_id: helper.decrypt(data.region),
        };
        department.find({_id:req.user.department},function (err,dept_Data) {

            Unit.find({_id:dept_Data[0].unit_id},function (err,unit_Data) {


                if(unit_Data[0].unit_type == 0 )
                {
                    reporting_line.find({}, function (error, data) {
                        User.find({department:  req.user.department}, function (e, d) {
                            reporting_line.find({department_id: req.user.department}, function (err, rep_Data) {


                                edd_json.approval = [];
                                for (ed in d) {

                                    //console.log(d[ed]._id);
                                    if (d[ed].is_manager == "true") {


                                        edd_json.approval.push({
                                            userid: d[ed]._id,
                                            status: 0,
                                            is_attachment: rep_Data[0].is_attachment,
                                            questions: rep_Data[0].questions,

                                        });
                                    }
                                    if(req.user.is_manager == "true")
                                    {
                                        edd_json.approval.push({
                                            userid: req.user.id,
                                            status: 0,
                                            is_approval: 0,
                                            is_rejected: 0,
                                            rejected_reason: "",
                                            is_attachment: rep_Data[0].is_attachment,
                                            questions: rep_Data[0].questions,
                                        });
                                    }
                                    else{
                                        edd_json.approval.push({
                                            userid: req.user.id,
                                            status: 1,
                                            is_approval: 1,
                                            is_rejected: 2,
                                            rejected_reason: ""
                                        });
                                    }
                                }





                                var edd = new Edd(edd_json);

                                customer.save().then((customerDoc) => {

                                    edd.save().then((eddDoc) => {

                                        req.flash('info', "Successfully added.");
                                        res.redirect('/admin/customer_edd');

                                    }, (e) => {
                                        req.flash('error', e);
                                        res.redirect('/admin/customer_edd');

                                    });
                                });
                            });
                        });
                    }).sort({"sort": -1});
                }
                else{
                    reporting_line.find({}, function (error, data) {


                        User.find({department: req.user.department}, function (e, d) {
                            reporting_line.find({department_id: req.user.department}, function (err, rep_Data) {

                                edd_json.approval = [];
                                for (ed in d) {


                                    if (d[ed].is_manager == "true") {


                                        edd_json.approval.push({
                                            userid: d[ed]._id,
                                            status: 0,
                                            is_attachment: rep_Data[0].is_attachment,
                                            questions: rep_Data[0].questions,

                                        });
                                    }
                                    if(req.user.is_manager == "true")
                                    {
                                        edd_json.approval.push({
                                            userid: req.user.id,
                                            status: 0,
                                            is_approval: 0,
                                            is_rejected: 0,
                                            rejected_reason: "",
                                            is_attachment: rep_Data[0].is_attachment,
                                            questions: rep_Data[0].questions,
                                        });
                                    }
                                    else{
                                        edd_json.approval.push({
                                            userid: req.user.id,
                                            status: 1,
                                            is_approval: 1,
                                            is_rejected: 2,
                                            rejected_reason: ""
                                        });
                                    }
                                }





                                var edd = new Edd(edd_json);

                                customer.save().then((customerDoc) => {

                                    edd.save().then((eddDoc) => {

                                        req.flash('info', "Successfully added.");
                                        res.redirect('/admin/customer_edd');

                                    }, (e) => {
                                        req.flash('error', e);
                                        res.redirect('/admin/customer_edd');
                                        console.log(e);
                                    });
                                });
                            });
                        });

                    }).sort({"sort": 1});
                }
            });
            //  })


        });


        // dept.forEach(async function(data){

        //  var u = await user.find({department:data._id});
        //  console.log(u);
        //  })


    }
})

router.get('/approval_log/:id', function (req, res) {
    try {


        edd_id = helper.decrypt(req.params.id);
        if (!edd_id || edd_id === null || edd_id.length === undefined)
        {
            req.flash('error', "Unable to process your request.");
            res.redirect('/admin/customer_edd');
        }
        else{
            Edd.find({_id: edd_id}, function (err, edd_data) {

                getdata = [];
                for (index in edd_data) {
                    for (approval_index in edd_data[index].approval) {
                        getdata.push(edd_data[index].approval[approval_index].userid)
                    }
                }
                User.find({
                    _id: {
                        $in: getdata
                    }
                }, function (err, udata) {
                    for (uindex in udata) {
                        for (approval_index in edd_data[0].approval) {
                            if (edd_data[0].approval[approval_index].userid == udata[uindex]._id) {
                                udata[uindex].edd_status = edd_data[0].approval[approval_index].status
                                udata[uindex].edd_rejected = edd_data[0].approval[approval_index].is_rejected
                                udata[uindex].edd_rejected_reason = edd_data[0].approval[approval_index].rejected_reason
                            }
                        }
                    }
                    res.render('admin/customer_edd/approval_log', {
                        layout: 'admin/layouts/default', users: req.user.role_id,username:req.user.username,
                        approval_log: udata,
                        edd_data: edd_data,
                        usrdata: udata,


                    })

                })
            })
        }
    }catch (e) {
        redirect('/admin/customer_edd');
    }
});

router.get('/:id?', function (req, res, next) {

    var perPage = 10;
    var page = req.param('page') || 1;
    id = req.params.id;

    where = {
        is_deleted: false,
    }
    if (id != undefined) {
        id = helper.decrypt(id);
        where._id = id;
    }

    Edd.find({"approval.userid": req.user.id}).sort({'date_added': 'desc'})
        .populate('customer_id')
        .skip((perPage * page) - perPage)
        .limit(perPage)
        .where(where)
        .lean()
        .exec((err, doc) => {

            if (err) {
                req.flash('error', "Unable to process your request.");
                res.redirect('/admin/customer_edd');
            }
            else {

                Edd.count().where(where).exec(function (err, count) {

                    for (index in doc) {
                        doc[index].customer_name = "";
                        if (doc[index].customer_id != null) {
                            doc[index].customer_name = doc[index].customer_id.customer_log[0].customer_name;
                            doc[index].national_identity = doc[index].customer_id.customer_log[0].national_identity;
                        }
                    }
                    for (edds_index in doc) {
                        for (approval_index in doc[edds_index].approval) {
                            if (doc[edds_index].approval[approval_index].userid == req.user.id) {

                                doc[edds_index].approval_status = doc[edds_index].approval[approval_index].status;
                                doc[edds_index].is_rejected = doc[edds_index].approval[approval_index].is_rejected;
                                doc[edds_index].is_attachment = doc[edds_index].approval[approval_index].is_attachment;
                                doc[edds_index].questions = doc[edds_index].approval[approval_index].questions;
                                /* doc[edds_index].questions = doc[edds_index].approval[approval_index].questions;*/

                            }
                        }
                    }
                    User.find({_id:req.user.id},function (err,u_data) {

                        department.find({_id:u_data[0].department},function(err,dept_data){

                            Unit.find({_id:dept_data[0].unit_id},function (err,unit_data) {




                                res.render('admin/customer_edd/index', {
                                    layout: 'admin/layouts/default',unit_type:unit_data,is_manager:req.user.is_manager, users: req.user.role_id,username:req.user.username, user_id: req.user.id, moment: moment,
                                    institutes: helper.manipulate(doc,
                                        ['_id'],
                                        ['is_active', 'is_deleted']
                                    ),
                                    current: page,
                                    pages: Math.ceil(count / perPage)

                                });

                            });
                        });
                    })
                })
            }

        });


});

router.get('/get_questions/:id', function (req, res, next) {
    var id = helper.decrypt(req.params.id);
    Edd.findById(id, function (err, data) {
        if (!err) {
            if (data) {
                var questions;
                for (aindex in data.approval) {
                    if (data.approval[aindex].userid == req.user.id) {
                        if (data.approval[aindex].questions) {
                            questions = data.approval[aindex].questions;
                        }
                    }
                }
                res.send(questions);
            }
        }
    })
});
router.post('/questions/:id', function (req, res, next) {
    try {
        id = helper.decrypt(req.params.id);
        if (!id || id === null || id.length === undefined) {
            res.send({
                status: false,
                message: "Unable to process your request."
            });
        } else {
            Edd.findById(id, function (err, d) {
                if (!d) {
                    res.send({
                        status: false,
                        message: "Unable to process your request."
                    });
                } else {

                    _.map(d.approval, (item, index) => {
                        d.approval[index].status = 1;
                        if (d.approval[index].userid == req.user.id) {
                            d.approval[index].is_approval = 1;
                            d.approval[index].aproval_date = new Date();

                            if(req.body.answer != undefined) {
                                for (answer_index in d.approval[index].questions) {
                                        d.approval[index].questions[answer_index].answer = req.body.answer[answer_index];
                                }
                            }
                        }
                    });
                    if (d.approval.length > 0) {
                        User.find({_id: req.user.id}, function (err, userData) {
                            reporting_line.find({
                                department_id: userData[0].department
                            }, function (error, repData) {
                                reporting_line.find({
                                    sort: {
                                        $ne: repData[0].sort,
                                        $gt: repData[0].sort
                                    }
                                }, function (err, reportingData) {

                                    if (reportingData.length == 0) {
                                        d.save(function (err) {
                                            if (err) {
                                                res.send({
                                                    status: false,
                                                    message: "Unable to process your request."
                                                });
                                            } else {
                                                res.send({
                                                    status: true,
                                                    message: "Successfully Approved"
                                                });
                                            }
                                        });
                                    } else {
                                        User.find({department: reportingData[0].department_id}, function (err, usrData) {
                                            for (userIndex in usrData) {
                                                if (usrData[userIndex].is_manager == "true") {
                                                    d.approval.push({
                                                        userid: usrData[userIndex]._id,
                                                        status: 0,
                                                        is_attachment: reportingData[0].is_attachment,
                                                        questions: reportingData[0].questions,
                                                    });
                                                }
                                            }
                                            d.save(function (err) {
                                                if (err) {
                                                    res.send({
                                                        status: false,
                                                        message: "Unable to process your request."
                                                    });
                                                } else {
                                                    res.send({
                                                        status: false,
                                                        message: "Successfully Approved."
                                                    });
                                                }
                                            });
                                        });
                                    }

                                }).sort({"sort": 1});

                            });
                        })
                    }

                }
                reporting_line.find({}, function (err, reportingData) {
                    User.find({department: reportingData[0].department_id}, function (err, repdata) {
                        for (index in repdata) {
                            if (repdata[index]._id == req.user.id) {
                                d.status = 1;
                                d.save();
                            }
                        }
                    })
                }).sort({"sort": -1});
            });
        }
    }
    catch (e) {
        res.send({
            status: false,
            message: "Unable to process your request."
        });
    }
});
/* GET View Details Edd Customer. */
router.get('/view/:id', function (req, res, next) {
    try {


        id = helper.decrypt(req.params.id);
        if (!id || id === null || id.length === undefined) {
            req.flash('error', "Unable to process your request.");
            res.redirect('/admin/customer_edd');
        } else {
            Edd.findById(id, function (err, data) {


                customers.findById(data.customer_id, function (err, customer_data) {
                    customer_type.findById(customer_data.customer_log[0].customer_type, function (err, customer_type_data) {
                        service_type.findById(customer_data.customer_log[0].ocupation.type_of_service, function (err, customer_service_type) {
                            pep_type.findById(customer_data.customer_log[0].ocupation.pep_type, function (err, customer_pep_type) {
                                fund_source.findById(customer_data.customer_log[0].source_of_fund.source, function (err, customer_source_of_fund) {
                                    // _.map(data.reason_of_edd,(item, index)=>{

                                    //     reason_edd.findById({_id:item},function(err,reason_edd_data){
                                    //     });
                                    // });
                                    reason_edd.find({
                                        '_id': {
                                            $in: data.reason_of_edd
                                        }
                                    }, function (err, reason) {
                                        if (!data) {
                                            req.flash('error', "Unable to process your request.");
                                            res.redirect('/admin/customer_edd');
                                        } else {
                                            res.render('admin/customer_edd/view', {
                                                layout: 'admin/layouts/default',
                                                moment: moment,
                                                customer: customer_data.customer_log[0],
                                                users: req.user.role_id,
                                                username:req.user.username,
                                                edd: data,
                                                customer_type: customer_type_data,
                                                customer_service_types: customer_service_type,
                                                customer_pep_types: customer_pep_type,
                                                customer_source_of_funds: customer_source_of_fund,
                                                reason_of_edd: reason

                                            })
                                        }
                                    })


                                });
                            });
                        });

                    });
                });
            });
        }

    } catch (e) {
        redirect('/admin/customer_edd');
    }
});

router.post("/approval/:id", upload.array('attachments', 12), function (req, res, next) {

    try {
        id = helper.decrypt(req.params.id);

        if (!id || id === null || id.length === undefined) {
            res.send({
                status: false,
                message: "Unable to process your request."
            });
        } else {

            Edd.findById(id, function (err, d) {
                if (!d) {
                    res.send({
                        status: false,
                        message: "Unable to process your request."
                    });
                } else {



                    _.map(d.approval, (item, index) => {
                        d.approval[index].status = 1;
                        if (d.approval[index].userid == req.user.id) {
                            d.approval[index].is_approval = 1;

                            d.approval[index].aproval_date = new Date();

                            var attach = [];
                            req.files.forEach(function (filname, index) {
                                attach.push(filname.filename);
                            })

                            d.approval[index].attachment = attach;

                            if(req.body.answer != undefined) {

                                for (answer_index in d.approval[index].questions) {


                                        d.approval[index].questions[answer_index].answer  = req.body.answer[answer_index];

                                }

                            }
                        }
                    });
                    // console.log(d);
                    // return;

                    if (d.approval.length > 0) {
                        User.find({_id: req.user.id}, function (err, userData) {

                            reporting_line.find({
                                department_id: userData[0].department
                            }, function (error, repData) {

                                reporting_line.find({
                                    sort: {
                                        $ne: repData[0].sort,
                                        $gt: repData[0].sort
                                    }

                                }, function (err, reportingData) {

                                    if (reportingData.length == 0) {

                                        d.save(function (err) {
                                            if (err) {
                                                res.send({
                                                    status: false,
                                                    message: "Unable to process your request."
                                                });
                                            } else {
                                                res.send({
                                                    status: true,
                                                    message: "Successfully Approved"
                                                });
                                            }
                                        });
                                    } else {

                                        User.find({department: reportingData[0].department_id}, function (err, usrData) {


                                            for (userIndex in usrData) {
                                                if(usrData[userIndex].is_manager == "true"){
                                                    d.approval.push({
                                                        userid: usrData[userIndex]._id,
                                                        status: 0,
                                                        is_attachment: reportingData[0].is_attachment,
                                                        questions: reportingData[0].questions,
                                                    });
                                                }
                                            }

                                            d.save(function (err) {
                                                if (err) {
                                                    res.send({
                                                        status: false,
                                                        message: "Unable to process your request."
                                                    });
                                                } else {
                                                    res.send({
                                                        status: true,
                                                        message: "Successfully Approved"
                                                    });
                                                }
                                            });
                                        });
                                    }

                                }).sort({"sort": 1});

                            });
                        })
                    }

                }
                reporting_line.find({}, function (err, reportingData) {

                    User.find({department: reportingData[0].department_id}, function (err, repdata) {

                        for (index in repdata) {

                            if (repdata[index]._id == req.user.id) {

                                d.status = 1;
                                d.save();
                            }
                        }
                    })

                }).sort({"sort": -1});

            });

        }
    }
    catch (e) {
        res.send({
            status: false,
            message: "Unable to process your request."
        });
    }

})
/* Approval Edd Customer. */
router.get("/approval/:id", function (req, res, next) {
    try {


        id = helper.decrypt(req.params.id);
        if (!id || id === null || id.length === undefined) {
            req.flash('error', "Unable to process your request.");
            res.redirect('/admin/customer_edd');
        } else {

            Edd.findById(id, function (err, d) {

                if (!d) {
                    req.flash('error', "Unable to process your request.");
                    res.redirect('/admin/customer_edd');
                } else {

                    _.map(d.approval, (item, index) => {
                        d.approval[index].status = 1;

                        if (d.approval[index].userid == req.user.id) {
                            d.approval[index].is_approval = 1;
                            d.approval[index].aproval_date = new Date();
                        }

                    });


                    if (d.approval.length > 0) {
                        User.find({_id: req.user.id}, function (err, userData) {

                            reporting_line.find({
                                department_id: userData[0].department
                            }, function (error, repData) {

                                reporting_line.find({
                                    sort: {
                                        $ne: repData[0].sort,
                                        $gt: repData[0].sort
                                    }

                                }, function (err, reportingData) {

                                    if (reportingData.length == 0) {

                                        d.save(function (err) {
                                            if (err) {
                                                req.flash('error', "Unable to process your request.");
                                                res.redirect('/admin/customer_edd');
                                            } else {
                                                req.flash('info', "Successfully Approval.");
                                                res.redirect('/admin/customer_edd');
                                            }
                                        });
                                    } else {
                                        User.find({department: reportingData[0].department_id}, function (err, usrData) {

                                            for (userIndex in usrData) {
                                                if (usrData[userIndex].is_manager == "true") {
                                                    d.approval.push({
                                                        userid: usrData[userIndex]._id,
                                                        status: 0,
                                                        is_attachment: reportingData[0].is_attachment,
                                                        questions: reportingData[0].questions,
                                                    });
                                                }
                                            }

                                            d.save(function (err) {
                                                if (err) {
                                                    req.flash('error', "Unable to process your request.");
                                                    res.redirect('/admin/customer_edd');
                                                } else {
                                                    req.flash('info', "Successfully Approval.");
                                                    res.redirect('/admin/customer_edd');
                                                }
                                            });
                                        });
                                    }

                                }).sort({"sort": 1});

                            });
                        })
                    }

                }
                reporting_line.find({}, function (err, reportingData) {

                    User.find({department: reportingData[0].department_id}, function (err, repdata) {
                        console.log(repdata,"dsfsdfds");
                        for (index in repdata) {

                            if (repdata[index]._id == req.user.id) {

                                d.status = 1;
                                d.save();
                            }
                        }
                    })

                }).sort({"sort": -1});

            });

        }
    }
    catch (e) {
        redirect('/admin/customer_edd');
    }

});
/* Rejected Edd Customer. */
router.post("/rejected/:id", function (req, res, next) {
    try {


        data = req.body;
        console.log(data.rejected_reason,">>>>>>>>");
        id = helper.decrypt(req.params.id);
        if (!id || id === null || id.length === undefined) {
            req.flash('error', "Unable to process your request.");
            res.redirect('/admin/customer_edd');
        } else {

            Edd.findById(id, function (err, d) {

                if (!d) {
                    req.flash('error', "Unable to process your request.");
                    res.redirect('/admin/customer_edd');
                } else {
                    d.status = 2;
                    _.map(d.approval, (item, index) => {

                        if (d.approval[index].userid == req.user.id) {
                            d.approval[index].is_rejected = 2;
                            d.approval[index].rejected_reason = data.rejected_reason;
                        }
                    });
                    d.save(function (err) {
                        if (err) {
                            req.flash('error', "Unable to process your request.");
                            res.redirect('/admin/customer_edd');
                        } else {
                            req.flash('info', "Successfully Approval.");
                            res.redirect('/admin/customer_edd');
                        }
                    });

                }

            });

        }
    }
    catch (e) {
        redirect('/admin/customer_edd');
    }
});


module.exports = router;