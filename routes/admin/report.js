var express = require('express');
const mongoose = require('mongoose');
var router = express.Router();
var Edd = require('../../models/admin/edd');
var customers = require('../../models/admin/customer');
var customer_type = require('../../models/admin/customer_type');
var reason_edd = require('../../models/admin/reason_edd');
var service_type = require('../../models/admin/service_type');
var pep_type = require('../../models/admin/pep_type');
var fund_source = require('../../models/admin/fund_source');
var User = require('../../models/admin/user');
var reporting_line = require('../../models/admin/reporting_line');
var helper = require('../../helpers/general');
var validation = require('../../helpers/validation');
var _ = require('lodash');
var fs = require('fs');
var excel = require('node-excel-export');
var async = require('async');


/* POST Add. */
router.post('/export', async function (req, res, next) {

// You can define styles as json object
    const styles = {
        headerDark: {
            fill: {
                fgColor: {
                    rgb: '8bc34a'
                }
            },
            font: {
                color: {
                    rgb: 'FFFFFFFF'
                },
                sz: 14,
                bold: true,
            }
        },
        cellPink: {
            fill: {
                fgColor: {
                    rgb: 'FFFFFF'
                }
            }
        },
        cellGreen: {
            fill: {
                fgColor: {
                    rgb: 'FF00FF00'
                }
            }
        }
    };

//Array of objects representing heading rows (very top)
    const heading = [];

//Here you specify the export structure
    const specification = {
        form_date: {
            displayName: 'Form Date',
            headerStyle: styles.headerDark,
            width: 100 // <- width in pixels
        },
        voucher_no: {
            displayName: 'Voucher Number',
            headerStyle: styles.headerDark,
            width: 100 // <- width in pixels
        },
        customer_name: {
            displayName: 'Customer Name',
            headerStyle: styles.headerDark,
            width: 100 // <- width in pixels
        },
        father_name: {
            displayName: 'Customer Father Name',
            headerStyle: styles.headerDark,
            width: 100 // <- width in pixels
        },
        national_identity: {
            displayName: 'national identity',
            headerStyle: styles.headerDark,
            width: 100 // <- width in pixels
        },
        telephone: {
            displayName: 'telephone',
            headerStyle: styles.headerDark,
            width: 100 // <- width in pixels
        },
        ntn: {
            displayName: 'ntn',
            headerStyle: styles.headerDark,
            width: 100 // <- width in pixels
        },
        nature_of_transaction: {
            displayName: 'Nature of transaction',
            headerStyle: styles.headerDark,
            width: 100 // <- width in pixels
        },

        amount_in_figure: {
            displayName: 'Amount in figure',
            headerStyle: styles.headerDark,
            width: 100 // <- width in pixels
        },
        amount_in_words: {
            displayName: 'Amount in words',
            headerStyle: styles.headerDark,
            width: 100 // <- width in pixels
        },
        customer_type: {
            displayName: 'Customer type',
            headerStyle: styles.headerDark,
            width: 100 // <- width in pixels
        },
        reason_of_edd: {
            displayName: 'reason of edd',
            headerStyle: styles.headerDark,
            width: 100 // <- width in pixels
        },
        type_of_service: {
            displayName: 'type of service',
            headerStyle: styles.headerDark,
            width: 100 // <- width in pixels
        },
        nature_of_profession: {
            displayName: 'nature of profession',
            headerStyle: styles.headerDark,
            width: 100 // <- width in pixels
        },
        organization_name: {
            displayName: 'organization name',
            headerStyle: styles.headerDark,
            width: 100 // <- width in pixels
        },
        pep_type: {
            displayName: 'pep type',
            headerStyle: styles.headerDark,
            width: 100 // <- width in pixels
        },
        associates: {
            displayName: 'Associates',
            headerStyle: styles.headerDark,
            width: 100 // <- width in pixels
        },
        others: {
            displayName: 'Others',
            headerStyle: styles.headerDark,
            width: 100 // <- width in pixels
        },
        relation_with_receiver: {
            displayName: 'relation with receiver',
            headerStyle: styles.headerDark,
            width: 100 // <- width in pixels
        },
        relation_with_receiver_occupation: {
            displayName: 'relation with receiver occupation',
            headerStyle: styles.headerDark,
            width: 100 // <- width in pixels
        },
        authorized_person_name: {
            displayName: 'authorized person name',
            headerStyle: styles.headerDark,
            width: 100 // <- width in pixels
        },
        authorized_person_relation: {
            displayName: 'authorized person relation',
            headerStyle: styles.headerDark,
            width: 100 // <- width in pixels
        },
        authorized_person_national_id: {
            displayName: 'authorized person national_id',
            headerStyle: styles.headerDark,
            width: 100 // <- width in pixels
        },
        is_authorized: {
            displayName: 'is authorized',
            headerStyle: styles.headerDark,
            width: 100 // <- width in pixels
        },
        source_of_fund: {
            displayName: 'source of fund',
            headerStyle: styles.headerDark,
            width: 100 // <- width in pixels
        },
        source_of_fund_others: {
            displayName: 'source of fund others',
            headerStyle: styles.headerDark,
            width: 100 // <- width in pixels
        },
        previous_transactions_type: {
            displayName: 'previous transactions type',
            headerStyle: styles.headerDark,
            width: 100 // <- width in pixels
        },
        previous_transactions_value: {
            displayName: 'previous transactions value',
            headerStyle: styles.headerDark,
            width: 100 // <- width in pixels
        },

        customer_feedback: {
            displayName: 'customer feedback',
            headerStyle: styles.headerDark,
            width: 100 // <- width in pixels
        },
    }



        Edd.find({
            form_date: {
                $gte: req.body.start_date,
                $lt: req.body.end_date,
               }
        })
            .populate('customer_id')
            .exec(function (err, edd_Data) {

                var dataset = [];
                async.eachSeries(edd_Data, function iterator(data, callback) {
                    customer_type.findById(data.customer_id.customer_log[0].customer_type, function (err, customer_type_name) {
                        service_type.findById(data.customer_id.customer_log[0].ocupation.type_of_service, function (err, service_type) {
                            pep_type.findById(data.customer_id.customer_log[0].ocupation.pep_type, function (err, pep_type) {
                                fund_source.findById(data.customer_id.customer_log[0].source_of_fund.source, function (err, source_of_fund) {
                                    reason_edd.find({_id: {$in: data.reason_of_edd}}, function (err, reason_of_edd_name) {
                                        var reasons = [];
                                        for(index in reason_of_edd_name)
                                        {
                                            reasons.push(reason_of_edd_name[index].name);
                                        }
                                        console.log(reasons);
                                        dataset.push({
                                            form_date: data.form_date,
                                            voucher_no: data.voucher_no,
                                            customer_name: data.customer_id.customer_log[0].customer_name,
                                            father_name: data.customer_id.customer_log[0].father_name,
                                            national_identity: data.customer_id.customer_log[0].national_identity,
                                            telephone: data.customer_id.customer_log[0].telephone,
                                            ntn: data.customer_id.customer_log[0].ntn,
                                            nature_of_transaction: data.nature_of_transaction,

                                            amount_in_figure: data.amount_in_figure,
                                            amount_in_words: data.amount_in_words,
                                            customer_type: customer_type_name.name,
                                            reason_of_edd: reasons,
                                            type_of_service: service_type.name,
                                            nature_of_profession: data.customer_id.customer_log[0].ocupation.nature_of_profession,
                                            organization_name: data.customer_id.customer_log[0].ocupation.organization_name,
                                            pep_type: pep_type.name,
                                            associates: data.customer_id.customer_log[0].ocupation.associates,
                                            others: data.customer_id.customer_log[0].ocupation.others,
                                            relation_with_receiver: data.customer_id.customer_log[0].relation_with_receiver.relation,
                                            relation_with_receiver_occupation: data.customer_id.customer_log[0].relation_with_receiver.occupation,
                                            authorized_person_name: data.customer_id.customer_log[0].authorized_person.name,
                                            authorized_person_relation: data.customer_id.customer_log[0].authorized_person.relation,
                                            authorized_person_national_id: data.customer_id.customer_log[0].authorized_person.national_id,
                                            is_authorized: data.customer_id.customer_log[0].authorized_person.is_authorized,
                                            source_of_fund: source_of_fund.name,
                                            source_of_fund_other: data.customer_id.customer_log[0].source_of_fund.other,
                                            previous_transactions_type: data.previous_transactions.type,
                                            previous_transactions_value: data.previous_transactions.value,

                                            customer_feedback: data.customer_feedback,
                                        });
                                        callback();
                                    });
                                });
                            });
                        });
                    });
                }, function done() {
                    report = excel.buildExport(
                        [ // <- Notice that this is an array. Pass multiple sheets to create multi sheet report
                            {
                                name: 'Report', // <- Specify sheet name (optional)
                                heading: heading, // <- Raw heading array (optional)
                                specification: specification, // <- Report specification
                                data: dataset // <-- Report data
                            }
                        ]
                    );
                    res.attachment('report.xlsx');
                    res.send(report);
                });
            });

    /* edd = Edd.find({"date_added": {"$gte": req.body.start_date, "$lt": req.body.end_date}}).stream();
     var filename = "edd.csv";
     res.setHeader('Content-disposition', 'attachment; filename=edd.csv');
     res.setHeader('Content-Type', 'text/csv');

     var csvStream = csv.createWriteStream({headers: true}), writableStream = fs.createWriteStream(filename);
     csvStream.pipe(writableStream);

     edd.on('data', function (edd_data) {

         customers.find({
             _id: edd_data.customer_id,

         }, function (err, customer_data) {

             var customer = customer_data[0].customer_log[0];
             customer_type.findById(customer.customer_type, function (err, customer_type_name) {
                 service_type.findById(customer.ocupation.type_of_service, function (err, service_type) {
                     pep_type.findById(customer.ocupation.pep_type, function (err, pep_type) {
                         fund_source.findById(customer.source_of_fund.source, function (err, source_of_fund) {
                             csv
                                 .write([
                                     {
                                         form_date: edd_data.form_date,
                                         voucher_no: edd_data.voucher_no,
                                         customer_name: customer.customer_name,
                                         father_name: customer.father_name,
                                         national_identity: customer.national_identity,
                                         telephone: customer.telephone,
                                         ntn: customer.ntn,
                                         nature_of_transaction: edd_data.nature_of_transaction,
                                         beneficiary_name: customer.beneficiary_detail.name,
                                         beneficiary_relationship: customer.beneficiary_detail.relationship,
                                         amount_in_figure: edd_data.amount_in_figure,
                                         amount_in_words: edd_data.amount_in_words,
                                         customer_type: customer_type_name.name,
                                         reason_edd: customer.national_identity,
                                         service_type: service_type.name,
                                         nature_of_profession: customer.ocupation.nature_of_profession,
                                         organization_name: customer.ocupation.organization_name,
                                         pep_type: pep_type.name,
                                         associates: customer.ocupation.associates,
                                         ocupation_Other: customer.ocupation.others,
                                         relation_with_receiverrelation: customer.relation_with_receiver.relation,
                                         relation_with_receiver_occupation: customer.relation_with_receiver.occupation,
                                         authorized_person_name: customer.authorized_person.name,
                                         authorized_person_relation: customer.authorized_person.relation,
                                         authorized_person_national_id: customer.authorized_person.national_id,
                                         authorized_person_is_authorized: customer.authorized_person.is_authorized,
                                         source_of_fund: source_of_fund.name,
                                         source_of_fund_others: customer.source_of_fund.others,
                                         previous_transactions_type: edd_data.previous_transactions.type,
                                         previous_transactions_value: edd_data.previous_transactions.value,
                                         additional_documents: edd_data.additional_documents,
                                         customer_feedback: edd_data.customer_feedback,
                                     }

                                 ], {
                                     headers: true,
                                     transform: function (row) {
                                         return {
                                             form_date: row.form_date,
                                             voucher_no: row.voucher_no,
                                             customer_name: row.customer_name,
                                             father_name: row.father_name,
                                             national_identity: row.national_identity,
                                             ntn: row.ntn,
                                             nature_of_transaction: row.nature_of_transaction,
                                             beneficiary_name: row.beneficiary_name,
                                             beneficiary_relationship: row.beneficiary_relationship,
                                             amount_in_figure: row.amount_in_figure,
                                             amount_in_words: row.amount_in_words,
                                             customer_type: row.customer_type,
                                             reason_edd: row.reason_edd,
                                             service_type: row.service_type,
                                             nature_of_profession: row.nature_of_profession,
                                             organization_name: row.organization_name,
                                             pep_type: row.pep_type,
                                             associates: row.associates,
                                             ocupation_Other: row.ocupation_Other,
                                             relation_with_receiverrelation: row.relation_with_receiverrelation,
                                             relation_with_receiver_occupation: row.relation_with_receiver_occupation,
                                             authorized_person_name: row.authorized_person_name,
                                             authorized_person_relation: row.authorized_person_relation,
                                             authorized_person_national_id: row.authorized_person_national_id,
                                             authorized_person_is_authorized: row.authorized_person_is_authorized,
                                             source_of_fund: row.source_of_fund,
                                             source_of_fund_others: row.source_of_fund_others,
                                             previous_transactions_type: row.previous_transactions_type,
                                             previous_transactions_value: row.previous_transactions_value,
                                             additional_documents: row.additional_documents,
                                             customer_feedback: row.customer_feedback,

                                         };
                                     }
                                 }).pipe(res);
                         })
                     });
                 });
             });


         });
     });*/
})
/* GET Index. */
router.get('/', function (req, res, next) {

    res.render('admin/report/index', {
        layout: 'admin/layouts/default', users: req.user.role_id,username:req.user.username,
    });
});


module.exports = router;