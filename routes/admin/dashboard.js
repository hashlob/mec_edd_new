var express = require('express');
var router = express.Router();

var helper = require('../../helpers/general');
var validation = require('../../helpers/validation');

//multer object creation

/* GET Add. */
router.get('/', function (req, res, next) {
    
    // Region.find().where({is_deleted: false}).lean().exec((err, doc) => {
        res.render('admin/dashboard', {
            layout: 'admin/layouts/default',users:req.user.role_id,username:req.user.username
        })
    // });
});

router.get('/logout', function (req, res) {
    req.logout()
    res.redirect('/admin/auth/login')
  })
module.exports = router;