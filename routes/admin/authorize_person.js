var express = require('express');
var router = express.Router();
var authorize_person = require('../../models/admin/authorize_person');
var helper = require('../../helpers/general');
var validation = require('../../helpers/validation');

//multer object creation
var multer = require('multer')
var storage = multer.diskStorage({
    destination: function (req, file, cb) {
        cb(null, 'public/uploads/')
    },
    filename: function (req, file, cb) {
        var extension = file.originalname.split('.');
        cb(null, Date.now() + "." + extension[1])
    }
})

var upload = multer({storage: storage})

/* GET Add. */
router.get('/add', function (req, res, next) {
    // Region.find().where({is_deleted: false}).lean().exec((err, doc) => {
        res.render('admin/authorize_person/form', {
            layout: 'admin/layouts/default',users:req.user.role_id,username:req.user.username,
        })
    // });
});

/* POST Add. */
router.post('/add', function (req, res, next) {
    data = req.body;
    var valid = validation.validate(data, {
        "name": {
            required: true
        },
        "relation": {
            required: true
        },
        "national_id": {
            required: true
        },
    });
    if (Object.keys(valid).length > 0) {
        error_message = "";
        for (var kv in valid) {
            for (var v in valid[kv]) {
                error_message += valid[kv][v] + "<br/>"
            }
        }
        req.flash('error', error_message);
        res.redirect('add');
    } else {
        var unit = new authorize_person({
            name: data.name,
            relation: data.relation,
            national_id: data.national_id,
         
            // region_id: helper.decrypt(data.region),
           
        });
        unit.save().then((doc) => {
            req.flash('info', "Successfully added.");
            res.redirect('/admin/authorize_person');
        }, (e) => {
            req.flash('error', e);
            res.redirect('authorize_person/add');
        });
    }
})

/* GET Index. */
router.get('/:id?', function (req, res, next) {
    
    var perPage = 10;
    var page = req.param('page') || 1;
    id = req.params.id;
    where = {
        is_deleted: false,
    }
    if (id != undefined) {
        id = helper.decrypt(id);
        where._id = id;
    }
    authorize_person.find().sort({'date_added': 'desc'})
        .skip((perPage * page) - perPage)
        .limit(perPage)
        .where(where)
        .lean()
        .exec((err, doc) => {
            if (err) {
                req.flash('error', "Unable to process your request.");
                res.redirect('/admin/authorize_person');
            } else {
                authorize_person.count().where(where).exec(function (err, count) {
                    res.render('admin/authorize_person/index', {
                        layout: 'admin/layouts/default',users:req.user.role_id,username:req.user.username,
                        institutes: helper.manipulate(doc,
                            ['_id'],
                            ['is_active', 'is_deleted']
                        ),
                        current: page,
                        pages: Math.ceil(count / perPage)
                    });
                });
            }
        });

});

/* GET Remove. */
router.get('/remove/:id', function (req, res, next) {
    try {
        id = helper.decrypt(req.params.id);
        if (id) {
            authorize_person.findById(id, function (err, data) {
                if (!data) {
                    req.flash('error', "Unable to process your request.");
                    res.redirect('/admin/authorize_person');
                } else {
                    data.is_deleted = true;
                    data.save(function (err) {
                        if (err) {
                            req.flash('error', "Unable to process your request.");
                            res.redirect('/admin/authorize_person');
                        } else {
                            req.flash('info', "Successfully deleted.");
                            res.redirect('/admin/authorize_person');
                        }
                    })
                }
            });
        }
    }
    catch (e) {
        redirect('/admin/authorize_person');
    }
});

/* GET View Details. */
router.get('/view/:id', function (req, res, next) {
    try {
        id = helper.decrypt(req.params.id);
        if (!id) {
            req.flash('error', "Unable to process your request.");
            res.redirect('/admin/authorize_person');
        } else {
            authorize_person.findById(id, function (err, data) {
                if (!data) {
                    req.flash('error', "Unable to process your request.");
                    res.redirect('/admin/authorize_person');
                } else {
                    res.render('admin/authorize_person/view', {
                        layout: 'admin/layouts/default', users: req.user.role_id,username:req.user.username,
                        institute: data
                    })
                }
            });
        }
    }
    catch (e) {
        redirect('/admin/authorize_person');
    }

});

/* GET Edit. */
router.get('/edit/:id', function (req, res, next) {
    try {
        id = helper.decrypt(req.params.id);
        if (!id) {
            req.flash('error', "Unable to process your request.");
            res.redirect('/admin/authorize_person');
        } else {
            authorize_person.findById(id, function (err, data) {

                res.render('admin/authorize_person/edit', {
                    layout: 'admin/layouts/default', users: req.user.role_id,username:req.user.username,

                    institute: data,
                    helper: helper,
                    id: helper.encrypt(data._id.toString())
                })

            })
        }
    }catch (e) {
        redirect('/admin/authorize_person');
    }
});

router.post("/edit/:id", function(req, res, next){
    try {
        id = helper.decrypt(req.params.id);
        if (!id) {
            req.flash('error', "Unable to process your request.");
            res.redirect('/admin/authorize_person');
        } else {
        data = req.body;
        console.log(data);

            authorize_person.findById(id, function (err, d) {
                if (!d) {
                    req.flash('error', "Unable to process your request.");
                    res.redirect('/admin/authorize_person');
                } else {
                    d.name = data.name;
                    d.relation = data.relation;
                    d.national_id = data.national_id;

                    // d.region_id = helper.decrypt(data.region);
                    d.save(function (err) {
                        if (err) {
                            req.flash('error', "Unable to process your request.");
                            res.redirect('/admin/authorize_person');
                        } else {
                            req.flash('info', "Successfully updated.");
                            res.redirect('/admin/authorize_person');
                        }
                    })
                }
            });
        }
    }
    catch (e) {
        redirect('/admin/authorize_person');
    }
})


module.exports = router;