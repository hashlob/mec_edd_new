var express = require('express');
var router = express.Router();


var Institute = require('../models/admin/unit');

var CompetitionScore = require('../models/competition_score');
var helper = require('../helpers/general');
var mongoose = require('mongoose');
var async = require("async");

/* GET Rounds by Competition ID. */
router.get('/rounds/:id', function(req, res, next) {
    var id = helper.decrypt(req.params.id);
    if(id){
        Round.find({competition_id: id}).lean().exec(function (err, rounds) {
            res.send({
                status: true,
                data: helper.manipulate(rounds, ['_id'], ['is_active', 'is_deleted', 'competition_id'])
            })
        })
    } else {
        res.send({
           status: false,
            message: "Unable to process your request."
        });
    }
});

/* GET Teams by Region ID. */
router.get('/teams/:id', function(req, res, next) {
    var id = helper.decrypt(req.params.id);
    if(id){
        Institute.find({region_id: id}).lean().exec(function (err, institutes) {
            res.send({
                status: true,
                data: helper.manipulate(institutes, ['_id'], ['is_active', 'is_deleted', 'region_id'])
            })
        })
    } else {
        res.send({
            status: false,
            message: "Unable to process your request."
        });
    }
});

router.get('/questions/:id/:type_id', function (req, res, next) {
    id = helper.decrypt(req.params.id);
    type_id = req.params.type_id;
    Question.find({ round_id : id, question_type_id: type_id })
        .populate('question_type_id','name')
        .lean()
        .exec(function (err, data) {
        res.send(helper.manipulate(data, ['_id', 'round_id'], ['is_active', 'is_deleted']));
    })
})

router.post('/score/save', function (req, res, next) {
    data = req.body;
    data.round_id = helper.decrypt(data.round_id);
    async.each(data.team, function (item, callback) {
        Institute.find({
            "access_code": item.code
        }, function(err, instituteData){
            data.team[data.team.indexOf(item)].institute_id = instituteData[0]._id;
            delete data.team[data.team.indexOf(item)].code;
            callback();
        });
    }, function (err) {
        var competition_score = new CompetitionScore(data);
        competition_score.save(function (err) {
            req.session.user_data.score_card = data;
            req.session.save(function () {
                res.send({
                    status: true,
                    message: "Successfully added"
                });
            })
        })
    });

})


module.exports = router;
