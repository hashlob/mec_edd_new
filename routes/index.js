var express = require('express');
var router = express.Router();

var Institute = require('../models/admin/unit');

var helper = require('../helpers/general');
var locals = require('../helpers/locals');
var rn = require('random-number');
var async = require("async");

var options = {
    min: 100000000
    , max: 500000000
    , integer: true
}

router.get('/test', function (req, res, next) {
    req.app.io.emit('request', {
        "code": "",
        "event": "start_quiz"
    });
})

/* GET home page. */
router.get('/', function (req, res, next) {
    res.render('auth', {
        layout: 'layouts/default',users:req.user.role_id
    })
});

router.get('/splash', function (req, res, next) {
    res.render('splash', {
        layout: 'layouts/default',users:req.user.role_id
    })
});

router.get('/selection', function (req, res, next) {
    res.render('user_selection', {
        layout: 'layouts/default',users:req.user.role_id
    })
});

router.get('/team-wait', function (req, res, next) {
    if(req.session.user_data.access_code == "" || req.session.user_data.access_code == undefined){
        res.redirect('/selection');
    }
    res.render('team_wait', {
        layout: 'layouts/default',users:req.user.role_id,
        code: req.session.user_data.access_code
    })
});

router.get('/controller', function (req, res, next) {
    if(req.session.user_data.teamConfig){
        regData = req.session.user_data.teamConfig;
        console.log(regData);
        Institute.find({
            _id: {$in: regData.team}
        }).populate('region_id').exec(function (err, data) {
            res.render('quiz_controller', {
                layout: 'layouts/default',users:req.user.role_id,
                institutes: data,
                round: regData.rounds
            })
        })
    }
});

router.get('/control/:code?', function (req, res, next) {
    var code = req.params.code;
    if (code != "" && code != undefined) {
        res.redirect('/audience/' + code);
    }
    res.render('control', {
        layout: 'layouts/default',users:req.user.role_id
    })
});

router.post('/control', function (req, res, next) {
    var data = req.body;
    if (data) {
        req.session.user_data = {
            access_code: data.access_code
        }
        req.session.save(function (err) {
            if (data['access_code'] == locals.socket_keys.admin) {
                res.redirect('/team');
            } else {
                res.redirect('/team-wait');
            }
        });
    }
});

router.get('/quiz', function (req, res, next) {
    var access_code = req.session.user_data.access_code;
    Institute.find({"access_code": access_code}, function (err, data) {
        console.log(data);
        res.render('quiz', {
            layout: 'layouts/default',users:req.user.role_id,
            institute: data
        });
    })
});

router.get('/audience/:code?', function (req, res, next) {
    var code = req.params.code;
    if (code == "" || code == undefined) {
        res.redirect('/splash');
    }
    res.render('audience', {
        layout: 'layouts/default',users:req.user.role_id,
        code: code || ""
    })
});

router.get('/team', function (req, res, next) {
    Competition.find().lean().exec(function (err, competitions) {
        Region.find().lean().exec(function (err, regions) {
            res.render('team', {
                layout: 'layouts/default',users:req.user.role_id,
                regions: helper.manipulate(regions, ['_id'], ['is_deleted', 'is_active']),
                competitions: helper.manipulate(competitions, ['_id'], ['is_deleted', 'is_active']),
            })
        })
    })
});

router.post('/confirm', function (req, res, next) {
    var data = req.body;
    if (req.session.user_data) {
        var insts = [];
        instititutes = helper.decrypt_array_values(data['team']);
        async.each(instititutes, function (item, callback) {
            Institute.findById(item).populate('region_id').exec(function (err, data) {
                var code = rn(options);
                data.access_code = code;
                data.save(function (err) {
                    insts.push(data);
                    callback();
                });
            });
        }, function (err) {
            data['team'] = insts;
            req.session.user_data.teamConfig = data;
            req.session.save(function () {
                res.render('confirm', {
                    layout: 'layouts/default',users:req.user.role_id,
                    institutes: helper.manipulate(insts, ['_id'], ['is_active'])
                })
            })
        });
    } else {
        res.redirect('/selection');
    }
});

module.exports = router;
