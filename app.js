var express = require('express');
var path = require('path');
var favicon = require('serve-favicon');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var mongoose = require('mongoose');
var db = require('./config/database');
var expressLayouts = require('express-ejs-layouts');
var flash = require('express-flash');
var session = require('express-session');
var passport = require('passport');
require('./config/passport')(passport)
//Admin Controllers Include
var admin_auth = require('./routes/admin/auth')(passport);
var admin_unit = require('./routes/admin/unit');
var admin_customer_type = require('./routes/admin/customer_type');
var admin_authorize_person = require('./routes/admin/authorize_person');
var admin_fund_source = require('./routes/admin/fund_source');
var admin_pep_type = require('./routes/admin/pep_type');
var admin_reason_edd = require('./routes/admin/reason_edd');
var admin_reporting_line = require('./routes/admin/reporting_line');
var admin_occupation = require('./routes/admin/occupation');
var admin_region = require('./routes/admin/region');
var admin_zone = require('./routes/admin/zone');
var admin_branch = require('./routes/admin/branch');
var admin_department = require('./routes/admin/department');
var admin_customer_edd = require('./routes/admin/customer_edd');
var admin_edd = require('./routes/admin/edd');
var admin_report = require('./routes/admin/report');
var admin_dashboard = require('./routes/admin/dashboard');
var index = require('./routes/index');
var api = require('./routes/api');

//Mongoose Configuration
mongoose.Promise = global.Promise;
mongoose.connect(db.database['mongo'].connection_string);

var app = express();

// view engine setup
app.set('views', path.join(__dirname, 'views'));

app.set('view engine', 'ejs');
app.use(expressLayouts);


// uncomment after placing your favicon in /public
//app.use(favicon(path.join(__dirname, 'public', 'favicon.ico')));
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
// app.use(cookieParser());
app.use(cookieParser('keyboard cat'));
app.use(session({ cookie: { maxAge: 6000000000 }, secret: "Shh, its a secret!"}));
app.use(flash());
app.use(express.static(path.join(__dirname, 'public')));


app.use(passport.initialize())
app.use(passport.session())
//Admin Controllers
app.use('/admin/auth', admin_auth);
app.use('/admin/unit', admin_unit);
app.use('/admin/customer_type', admin_customer_type);
app.use('/admin/authorize_person', admin_authorize_person);
app.use('/admin/fund_source', admin_fund_source);
app.use('/admin/pep_type', admin_pep_type);
app.use('/admin/reason_edd', admin_reason_edd);
app.use('/admin/reporting_line', admin_reporting_line);
app.use('/admin/occupation', admin_occupation);
app.use('/admin/region', admin_region);
app.use('/admin/zone', admin_zone);
app.use('/admin/branch', admin_branch);
app.use('/admin/department', admin_department);
app.use('/admin/customer_edd', admin_customer_edd);
app.use('/admin/edd/', admin_edd);
app.use('/admin/report', admin_report);
app.use('/', index);
app.use('/api', api);
app.use('/admin/dashboard', admin_dashboard);


// catch 404 and forward to error handler
app.use(function(req, res, next) {
  var err = new Error('Not Found');
  err.status = 404;
  next(err);
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});



module.exports = app;
