$(document).ready(function () {

    var socket = io();
    var interval;
    var score = 0;

    $.blockUI({
        baseZ: 9999999999999999999,
        message: "Please wait for your turn."
    });

    $(".btn_option").click(function () {
        $button = $(this);
        var is_correct = $button.find('.option_button').attr('data-correct');
        $(".btn_option").removeClass('option-bg-red');
        $(".btn_option").removeClass('option-bg-green');
        if(is_correct){
            $(this).toggleClass("option-bg-green");
            addScore();
        } else {
            $(this).toggleClass("option-bg-red");
            $("span[data-correct='true']").parent().parent().toggleClass("option-bg-green");
        }
        socket.emit('request', {
            "code": "admin_code",
            "event": "question_response",
            "data": {
                answer: $(this).find('.option_button').attr('data-id')
            }
        });
        lockQuestion();
    })

    socket.on($(".team_box").data('code'), function(msg){
        if(msg){
            if(msg['event'] == "question"){
                if(msg['data']){
                    getQuestion(msg['data']['question']);
                    $("#timer").text(30);
                    if(msg['data']['is_skip']){
                        $(".skip_question").hide();
                        $(".skip_question_cross").show();
                    }
                    if(msg['data']['time_reset']){
                        clearInterval(interval)
                        $("#timer").text(30);
                        $.blockUI({
                            baseZ: 9999999999999999999,
                            message: "Please wait for your turn."
                        });
                    }
                }
            }
            if(msg['event'] == "start_time"){
                interval = setInterval(function(){
                    var time = $("#timer").text()-1;
                    if(time >= 0){
                        $("#timer").text(time)
                    } else {
                        clearInterval(interval);
                        $.blockUI({
                            baseZ: 9999999999999999999,
                            message: "Time up, Please wait for your turn."
                        });
                    }
                },1000)
                $.unblockUI();
            }
        }
    });

    function getQuestion(question){
        $(".btn_option").each(function(){
            $(this).removeClass('option-bg-green')
            $(this).removeClass('option-bg-red');
        })
        if(question){
            $(".question").html(question.question);
            fillOptions(question.options);
            // $.unblockUI();
        }
        return false;
    }

    function fillOptions(options){
        $(".option_button").removeAttr('data-correct')
        $.each(options, function(i, d){
            if(d.is_correct){
                $(".option_button").eq(i).attr('data-correct', true);
            }
            $(".option_button").eq(i).attr('data-id', d._id);
            $(".option_button").eq(i).html(d.value);
        })
    }

    function lockQuestion(){
        clearInterval(interval);
        $.blockUI({
            baseZ: 9999999999999999999,
            message: "Please wait for your turn."
        });
    }
    
    function addScore() {
        score = parseInt($(".score_final").text()) + 10;
        $(".score_final").text(score);
    }
})