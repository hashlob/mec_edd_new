$(document).ready(function () {

    var socket = io();
    var current_question_index = 0;
    var selectedTeam = 0;
    var interval;
    var question_count = 0;
    var current_audience_question_index = 0;

    $.blockUI({
        baseZ: 9999999999999999999,
        message: "Loading questions."
    });

    $('div.qus-set').block({
        message: "",
        overlayCSS: {backgroundColor: 'rgba(255, 255, 255, 0.02)'}
    });

    $('div.helplines').block({
        message: "",
        overlayCSS: {backgroundColor: 'rgba(255, 255, 255, 0.02)'}
    });

    socket.on("admin_code", function (msg) {
        if (msg) {
            if (msg['event'] == "question_response") {
                console.log(msg);
                if (msg['data']) {
                    $(".option_button[data-id=" + msg['data']['answer'] + "]").parent().parent().trigger('click')
                    lockQuestion();
                }
            }
        }
    });

    var round_id = $(".q_round").val();

    if (round_id != "") {
        $.get("/api/questions/" + round_id + "/5a9b6bf4361d3b0a4457a19e", function (data) {
            if (typeof(Storage) !== "undefined") {
                localStorage.setItem("questions", JSON.stringify(data));
                getQuestion(current_question_index);
                $.unblockUI();
            } else {
                alert('Please update your browser')
            }
        })
        // Audience
        $.get("/api/questions/" + round_id + "/5a92d99b530e443e5c785d95", function (data) {
            if (typeof(Storage) !== "undefined") {
                localStorage.setItem("audience_questions", JSON.stringify(data));
            } else {
                alert('Please update your browser')
            }
        })

        // Tie Questions
        $.get("/api/questions/" + round_id + "/5a92d95c530e443e5c785d94", function (data) {
            if (typeof(Storage) !== "undefined") {
                localStorage.setItem("tie_questions", JSON.stringify(data));
            } else {
                alert('Please update your browser')
            }
        })
    }

    function init() {
        selectedTeam = $(".team_box").eq(0).data('code');
        $(".team_box").eq(0).trigger('click');
        var questions = JSON.parse(localStorage.getItem("questions"));
        broadcastQuestion(questions[current_question_index], selectedTeam);
    }

    $(".btn_option").click(function () {
        $button = $(this);
        var is_correct = $button.find('.option_button').attr('data-correct');
        $(".btn_option").removeClass('option-bg-red');
        $(".btn_option").removeClass('option-bg-green');
        if (is_correct) {
            $(this).toggleClass("option-bg-green");
            addScore();
        } else {
            $(this).toggleClass("option-bg-red");
        }

    });

    $(".team_box").click(function () {
        selectedTeam = $(this).data('code');
        $('.team_box').attr('data-active', false);
        $(this).attr('data-active', true);
        $(".teamName").css({
            color: "#fff"
        });

        $(this).find(".teamName").css({
            color: "red"
        })
    })

    function getQuestion(current_index) {
        var questions = JSON.parse(localStorage.getItem("questions"));
        $(".btn_option").each(function () {
            $(this).removeClass('option-bg-green')
            $(".btn_option").removeClass('option-bg-red');
        })
        if (questions.length > 0) {
            $(".question").html(questions[current_index].question);
            fillOptions(questions[current_index].options);
            $(".questionMusic")[0].play();
        }
        return false;
    }

    function getAudienceQuestion(current_index) {
        var questions = JSON.parse(localStorage.getItem("audience_questions"));
        $(".btn_option").each(function () {
            $(this).removeClass('option-bg-green')
            $(".btn_option").removeClass('option-bg-red');
        })
        if (questions.length > 0) {
            $(".question").html(questions[current_index].question);
            fillOptions(questions[current_index].options);
        }
        return false;
    }

    function fillOptions(options) {
        $(".option_button").removeAttr('data-correct');
        $.each(options, function (i, d) {
            if (d.is_correct) {
                $(".option_button").eq(i).attr('data-correct', true);
            }
            $(".option_button").eq(i).attr('data-id', d._id);
            $(".option_button").eq(i).html(d.value);
        })
    }

    $(".startTimer").click(function () {
        var questions = JSON.parse(localStorage.getItem("questions"));
        $('div.helplines').unblock();
        //broadcastQuestion(questions[current_question_index], selectedTeam);
        broadCastTime(selectedTeam);
        interval = setInterval(function () {
            var time = $("#timer").text() - 1;
            if (time >= 0) {
                $("#timer").text(time)
            }
        }, 1000);
        $(this).parent().css('display', 'none');
        $("#nextBTN").css('display', 'block');
    })

    $("#nextQuestion").click(function () {
        question_count++;

        if(question_count == 6 || question_count == 12){
            $(".psl_ring")[0].play();
            getAudienceQuestion(current_audience_question_index);
            current_audience_question_index++;
            return;
        }

        var questions = JSON.parse(localStorage.getItem("questions"));
        $('div.helplines').block({
            message: "",
            overlayCSS: {backgroundColor: 'rgba(255, 255, 255, 0.02)'}
        });
        if(questions.length == current_question_index) {
            var temp = [];
            $(".team_box").each(function(i, d){
                temp.push({
                    "code": $(this).data('code'),
                    "score": $(this).find('.score_final').text()
                });
            });
            $.post("/api/score/save", {
                "round_id": $(".q_round").val(),
                "team": temp
            }, function(d){
                if(d['status']){
                    scoreBoard();
                }
            })
        }
        current_question_index++;
        broadcastQuestion(questions[current_question_index], selectedTeam);
        getQuestion(current_question_index);
        $("#timer").text(30);
        $(this).parent().css('display', 'none');
        $("#startTimeBtn").css('display', 'block');
    })

    function broadcastQuestion(question, code) {
        if (question) {

            socket.emit('request', {
                "code": code,
                "event": "question",
                "data": {
                    question: question
                }
            });
        }
    }

    function broadcastQuestionAudience(question, code) {
        if (question) {
            socket.emit('request', {
                "code": code,
                "event": "audience_poll",
                "data": {
                    question: question
                }
            });
        }
    }

    function broadCastTime(code) {
        socket.emit('request', {
            "code": code,
            "event": "start_time",
        });
    }

    function lockQuestion() {
        clearInterval(interval);
    }

    function addScore() {
        score = parseInt($(".team_box[data-active=true]").find('.score_final').text()) + 10;
        $(".team_box[data-active=true]").find('.score_final').text(score);
    }

    function scoreBoard(){
        $(".quiz_screen").hide();
        $(".score_board").show();
        var team_a_score = $(".team_box").eq(0).find('.score_final').text();
        var team_b_score = $(".team_box").eq(1).find('.score_final').text();
        if(team_a_score > team_b_score){
            $(".institute_name").text($(".team_box").eq(0).find('.teamName').text());
            $(".quiz_score").text($(".team_box").eq(0).find('.score_final').text());
        } else {
            $(".institute_name").text($(".team_box").eq(1).find('.teamName').text());
            $(".quiz_score").text($(".team_box").eq(1).find('.score_final').text());
        }
    }
    
    $(".skip_question").click(function () {
        var code = $(this).parent().parent().parent().attr('data-code');
        var questions = JSON.parse(localStorage.getItem("questions"));
        current_question_index++;
        getQuestion(current_question_index);
        socket.emit('request', {
            "code": code,
            "event": "question",
            "data": {
                question: questions[current_question_index],
                is_skip: true,
                time_reset: true
            }
        });
        clearInterval(interval);
        $("#timer").text(30);
        $(this).parent().find('.skip_question').hide();
        $(this).parent().find('.skip_question_cross').show();
        $("#nextBTN").hide();
        $("#startTimeBtn").show();
    });

    $(".audience_poll").click(function(){
        var questions = JSON.parse(localStorage.getItem("questions"));
        broadcastQuestionAudience(questions[current_question_index], 'audience_code');
    });



    // Initialize
    init();

})