const config = require('../config/config');
const crypto = require('crypto');
const encryption = config.config.encryption;

module.exports = {
    "encrypt" : (value) => {
        var IV = new Buffer(crypto.randomBytes(16)); // ensure that the IV (initialization vector) is random
        var cipher_text;
        var hmac;
        var encryptor;

        encryptor = crypto.createCipheriv(encryption.ALGORITHM, encryption.KEY, IV);
        encryptor.setEncoding('hex');
        encryptor.write(value);
        encryptor.end();

        cipher_text = encryptor.read();

        hmac = crypto.createHmac(encryption.HMAC_ALGORITHM, encryption.HMAC_KEY);
        hmac.update(cipher_text);
        hmac.update(IV.toString('hex')); // ensure that both the IV and the cipher-text is protected by the HMAC

        // The IV isn't a secret so it can be stored along side everything else
        return cipher_text + "$" + IV.toString('hex') + "$" + hmac.digest('hex')
    },
    "decrypt" : (value) => {
        try {
            var cipher_blob = value.split("$");
            var ct = cipher_blob[0];
            var IV = new Buffer(cipher_blob[1], 'hex');
            var hmac = cipher_blob[2];
            var decryptor;

            chmac = crypto.createHmac(encryption.HMAC_ALGORITHM, encryption.HMAC_KEY);
            chmac.update(ct);
            chmac.update(IV.toString('hex'));

            if (!constant_time_compare(chmac.digest('hex'), hmac)) {
                console.log("Encrypted Blob has been tampered with...");
                return null;
            }

            decryptor = crypto.createDecipheriv(encryption.ALGORITHM, encryption.KEY, IV);
            var decryptedText = decryptor.update(ct, 'hex', 'utf-8');
            return decryptedText + decryptor.final('utf-8');
        } catch(ex) {
            return ex;
        }
    },
    "manipulate": (data, keys, rKeys) => {
        data.forEach((d, i) => {
            keys.forEach((kd, ki) => {
                data[i][kd] = module.exports.encrypt(data[i][kd].toString());
                if(data[i]['date_added']){
                    data[i]['date_added'] = module.exports.date_format(data[i]['date_added'].toString());
                }
            })
            rKeys.forEach((rkd, rki) => {
                delete data[i][rkd];
            })
        });
        return data;
    },
    "decrypt_array_values": (array) => {
        array.forEach((d, i) => {
            array[i] =  module.exports.decrypt(d);
        });
        return array;
    },
    "date_format": function formatDate(date) {
        var d = new Date(date),
            month = '' + (d.getMonth() + 1),
            day = '' + d.getDate(),
            year = d.getFullYear();

        if (month.length < 2) month = '0' + month;
        if (day.length < 2) day = '0' + day;

        return [day, month, year].join('-');
    }
}


constant_time_compare = function (val1, val2) {
    var sentinel;

    if (val1.length !== val2.length) {
        return false;
    }


    for (var i = 0; i <= (val1.length - 1); i++) {
        sentinel |= val1.charCodeAt(i) ^ val2.charCodeAt(i);
    }

    return sentinel === 0
}