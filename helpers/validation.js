module.exports = {
    "validate": (body, fields) => {
        var error = {};

        for(var key in fields) {
            for(param in fields[key]){
                if(param == "required"){
                    if(fields[key][param]){
                        if(body[key] == "" || body[key] == undefined){
                            if(!error[key]){
                                error[key] = []
                            }
                            error[key].push(key + " is required.");
                        }
                    }
                }
                //Checking type
                if(param == "type"){
                    if(fields[key][param] == "email"){
                        if(!isEmail(body[key])){
                            if(!error[key]){
                                error[key] = []
                            }
                            error[key].push(key + " is invalid.");
                        }
                    }
                }
            }
        }
        return error;
    }
}

var isEmail = (email) => {
    var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(String(email).toLowerCase());
}