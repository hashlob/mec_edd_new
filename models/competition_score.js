var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var competitionSscoreSchema = new Schema({
    "round_id" : String,
    "team" : [
        {
            score: String,
            institute_id: String
        }
    ],
    "date_added" : { type: Date, default: Date.now }
});

// create the model for users and expose it to our app
module.exports = mongoose.model('CompetitionScore', competitionSscoreSchema, 'competition_score');