var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var  fund_sourceSchema = new Schema({
    "name" : String,
    "is_active" : { type: Boolean, default: true },
    "is_deleted" : { type: Boolean, default: false },
    "date_added" : { type: Date, default: Date.now },
  
});

// create the model for users and expose it to our app
module.exports = mongoose.model('fund_source',  fund_sourceSchema, 'fund_source');