var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var departmentSchema = new Schema({
    "name" : String,
    "unit_id" : String,
    "is_active" : { type: Boolean, default: true },
    "is_deleted" : { type: Boolean, default: false },
    "date_added" : { type: Date, default: Date.now },
    "date_update" : { type: Date, default: Date.now },
  
});

// create the model for users and expose it to our app
module.exports = mongoose.model('department', departmentSchema, 'department');