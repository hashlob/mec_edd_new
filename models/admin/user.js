var mongoose = require('mongoose');
var bcrypt = require('bcrypt-nodejs');
var Schema = mongoose.Schema;

var userSchema = new Schema({
    "username" : String,
    "last_name" : String,
    "email" : String,
    "password" : String,
    "role_id" : { type: String, default: 1 },
     department:{
           type:String,
           required:true,
     },
     is_manager: {type:String},
    "is_active" : { type: Boolean, default: true },
    "is_deleted" : { type: Boolean, default: false },
    "date_added" : { type: Date, default: Date.now }
});

userSchema.methods.hashPassword = function (password) {
    return bcrypt.hashSync(password,bcrypt.genSaltSync(10))
}

userSchema.methods.comparePassword = function (password,hash) {
    return bcrypt.compareSync(password,hash)
}
// create the model for users and expose it to our app
module.exports = mongoose.model('User', userSchema, 'user');