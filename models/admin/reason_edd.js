var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var  reason_eddSchema = new Schema({
    "name" : String,
    "is_active" : { type: Boolean, default: true },
    "is_deleted" : { type: Boolean, default: false },
    "date_added" : { type: Date, default: Date.now },
  
});

// create the model for users and expose it to our app
module.exports = mongoose.model('reason_edd',  reason_eddSchema, 'reason_edd');