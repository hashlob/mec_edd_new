var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var departmentSchema = new Schema({
    "edd_id" : Number,
    "reason_edd_id" : Number,
   });

// create the model for users and expose it to our app
module.exports = mongoose.model('department', departmentSchema, 'department');