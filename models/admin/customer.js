var mongoose = require('mongoose');
var Schema = mongoose.Schema;



var customerLogSchema = new Schema({ 
    customer_name: {
        type: String,
       
    }, 
    father_name: {
        type: String,
        
    }, 
    national_identity: {
        type: String,
        
    }, 
    telephone: {
        type: String,
       
    },
    ntn: {
        type: String,

    },
    organization_Designation: {
        type: String,

    },
    organization_institution_name: {
        type: String,

    },
    organization_address: {
        type: String,

    },
    beneficiary_detail: {
        name: {
            type: String,
           
        }, 
        relationship: {
            type: String,
           
        }
    },
    customer_type: {
        type: String,
       
    },
    customer_type_non_resident_location:{
        type:String,
    },
    customer_type_foreginer_nationality:{
        type:String,
    },

    ocupation: {
        type_of_service: {
            type: String,
           
        },
        Other: {
            type: String,

        },
        nature_of_profession: {
            type: String,
           
        },
        organization_name: {
            type: String,
          
        },
        pep_type: {
            type: String,
            default: ''
           
        },
        pep_associates: {
            type: String,
            
        },
        pep_other: {
            type: String,
            
        }
    },
    relation_with_receiver: {
        relation : {
            type: String,
           
        }, 
        occupation : {
            type: String,

        }
    },
    authorized_person: {
        name: {
            type: String,
          
        }, 
        relation: {
            type: String,
            
        }, 
        national_id: {
            type: String,
          
        },
        beneficial_cell_number: {
            type: String,

        },

        beneficial_owner_occupation: {
            type: String,

        },
        beneficial_owner_Organization: {
            type: String,

        },
        beneficial_owner_Organization_address: {
            type: String,

        },
        beneficial_owner_cell_confirmation: {
            type: String,

        },

        beneficial_owner_ntn: {
            type: String,

        },

        beneficial_owner_designation: {
            type: String,

        },

        is_authorized: {
            type: String , default: 0
           
        }
    },
    source_of_fund: {
        source: {
            type: String,
           
        }, 
        others: {
            type: String,
            
        }
    },
    "is_active" : { type: Boolean, default: true },
    "is_deleted" : { type: Boolean, default: false },
    "date_added" : { type: Date, default: Date.now },
    "date_update" : { type: Date, default: Date.now },
   
});

var customerSchema = new Schema({
    national_identity: {
        type: [String],
        required: true
    },
    customer_log: {
        type: [customerLogSchema],
        required: true
    }
});

module.exports = mongoose.model('customer', customerSchema, 'customer');