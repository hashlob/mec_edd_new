var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var questions_Schema = new Schema({
    question:{
        type:String,
    },
    options:{
        type: [String],
    }
});
var  reporting_lineSchema = new Schema({
    "department_id" : String,
    "unit_id" : String,
    "sort" : String,
    "is_manager": {type:String},
    "is_attachment": { type: Boolean, default: false },
     questions:{
      type:[questions_Schema],
    },
    "is_active" : { type: Boolean, default: true },
    "is_deleted" : { type: Boolean, default: false },
    "date_added" : { type: Date, default: Date.now },
  
});

// create the model for users and expose it to our app
module.exports = mongoose.model('reporting_line',  reporting_lineSchema, 'reporting_line');