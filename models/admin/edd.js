var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var questions_Schema = new Schema({
    question:{
        type:String,
    },
    options:{
        type: [String],
    },
    answer:{
        type: String,
    },
});
var approvalSchema = new Schema({
    userid:{
        type: String
    },
    status:{
        type:Number,
    },
    is_attachment : {
        type: Boolean, default: false
    },
    attachment: [String],
    questions:{
        type:[questions_Schema],
    },
    is_approval:{
        type: Number, default: 0
    },
    aproval_date:{
        type: Date
    },
    is_rejected:{
        type: Number ,default: 0,
    },
    rejected_reason:{
        type: String,
    },

});
var additional_document_Schema = new Schema({
    name:{
        type:String,
    },
    document:{
        type: String,
    }
});

var eddSchema = new Schema({
    "branch_name":String,
    "user_id" : String,
    "customer_id" : { type: Schema.ObjectId, ref: 'customer', index: true },
    "form_date" : Date,
    "voucher_no" : String,
    "nature_of_transaction" : [String],
    "amount_in_figure" : String,
    "amount_in_words" : String,
    "reason_of_edd" : [String],
    approval: [approvalSchema],
    previous_transactions : {
        type: {
            type: String,
            required: true
        },
        value: {
            type: Number,
            required: true
        }
    },

    additional_documents : {
        type: [additional_document_Schema]


    },
    "customer_feedback" : String,

    "is_active" : { type: Boolean, default: true },
    "is_deleted" : { type: Boolean, default: false },
    "date_added" : { type: Date, default: Date.now },
    "date_update" : { type: Date, default: Date.now },
    "status" : { type: Number, default: 0 }

});

// create the model for users and expose it to our app
module.exports = mongoose.model('edd', eddSchema, 'edd');