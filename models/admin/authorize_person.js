var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var authorize_personSchema = new Schema({
    "name" : String,
    "relation" : String,
    "national_id" : String,
    "is_active" : { type: Boolean, default: true },
    "is_deleted" : { type: Boolean, default: false },
    "date_added" : { type: Date, default: Date.now },
});

// create the model for users and expose it to our app
module.exports = mongoose.model('authorize_person', authorize_personSchema, 'authorize_person');