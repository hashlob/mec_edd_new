var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var unitSchema = new Schema({
    "name" : String,
    "code" : String,
    "address" : String,
    "unit_type" : Number,
    "parent_unit" : String,
    "is_active" : { type: Boolean, default: true },
    "is_deleted" : { type: Boolean, default: false },
    "date_added" : { type: Date, default: Date.now },
    "date_update" : { type: Date, default: Date.now },
  
});

// create the model for users and expose it to our app
module.exports = mongoose.model('Unit', unitSchema, 'unit');